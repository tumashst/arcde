﻿using Model.Office;
using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels
{
    public class CartViewModel
    {
        public List<Product<Car>> Products { get; set; }

        [DisplayFormat(DataFormatString = "${0:n0}")]
        public int TotalPrice { get; set; }
        public int Count { get; set; }

        public CartViewModel(List<Product<Car>> products, int totalPrice, int count)
        {
            Products = products;
            TotalPrice = totalPrice;
            Count = count;
        }
    }
}
