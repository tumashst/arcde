﻿using Model.Office;

namespace Web.ViewModels;

public class IndexViewModel
{
    public CarProduct[] Products { get; }
    public PageViewModel PageViewModel { get; set; }
    public BuyFormViewModel BuyFormViewModel { get; }
    public SellFormViewModel SellFormViewModel { get; }
    public IndexViewModel(CarProduct[] products, PageViewModel pageViewModel)
    {
        Products = products;
        PageViewModel = pageViewModel;
        BuyFormViewModel = new(products);
        SellFormViewModel = new(products);
    }
}