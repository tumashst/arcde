﻿using Model.Office;

namespace Web.ViewModels;

public class BuyFormViewModel
{
    public string Mark { get; set; }
    public string Model { get; set; }
    public int Count { get; set; }

    public IReadOnlyCollection<string> Marks { get; }

    public IReadOnlyCollection<string> Models { get; }

    public int MaxCount { get; }
    public BuyFormViewModel(IReadOnlyCollection<CarProduct> carProducts)
    {

        Marks = carProducts.Select(carProduct => carProduct.Subject.Mark).Distinct().ToArray();

        Models = carProducts.Select(carProduct => carProduct.Subject.Model).Distinct().ToArray();

        MaxCount = carProducts.Any() ? carProducts.Max(carProduct => carProduct.Count) : 0;
    }
}