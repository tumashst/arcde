﻿using Model.Office;

namespace Web.ViewModels
{
    public class EditFormViewModel
    {
        public EngineType[] EngineTypes { get; }
        public Transmission[] Transmissions { get; }
        public Body[] Bodies { get; }

        public DriveUnit[] DriveUnits { get; }

        public EditFormViewModel(EngineType[] engineTypes, Transmission[] transmissions, Body[] bodies, DriveUnit[] driveUnits)
        {
            (EngineTypes, Transmissions, Bodies, DriveUnits) = (engineTypes, transmissions, bodies, driveUnits);
        }
        public int Id { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public int Count { get; set; }
        public int Price { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public Body Body { get; set; }
        public Engine Engine { get; set; }
        public DriveUnit DriveUnit { get; set; }
        public Transmission Transmission { get; set; }
        public Color Color { get; set; }
        public byte[] Image { get; set; }
    }
}
