﻿using Model.Office;

namespace Web.ViewModels;

public class AdminViewModel
{
    public AdminViewModel(string[] marks, CarProduct[] carProducts, DriveUnit[] driveUnits, Transmission[] transmissions, EngineType[] engineTypes, Body[] bodies)
    {
        Marks = marks;
        Products = carProducts;
        AddFormViewModel = new AddFormViewModel(engineTypes, transmissions, bodies, driveUnits);
        EditFormViewModel = new EditFormViewModel(engineTypes, transmissions, bodies, driveUnits);
    }

    public AddFormViewModel AddFormViewModel { get; set; }
    public EditFormViewModel EditFormViewModel { get; set; }
    public string[] Marks { get; }
    public CarProduct[] Products { get; }
}