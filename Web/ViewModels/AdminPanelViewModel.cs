﻿using Model.Office;

namespace Web.ViewModels;

public class AdminPanelViewModel
{
    public string OfficeName { get; set; }
    public CarProduct[] Products { get; set; }

    public AdminPanelViewModel(string officeName, CarProduct[] products)
    {
        OfficeName = officeName;
        Products = products;
    }

}