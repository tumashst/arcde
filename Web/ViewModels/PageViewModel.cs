﻿namespace Web.ViewModels
{
    public class PageViewModel
    {

        public int BorderBlockSize { get; } = 2;
        public int MiddleBlockSize { get; set; } = 5;
        public bool? EnablePagination { get; }
        public int HalfMiddleBlockSize => (int)Math.Ceiling((double)MiddleBlockSize / 2);
        private int MaxPageButtons => MiddleBlockSize + BorderBlockSize;

        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }

        public PageViewModel(int pageNumber, int pageSize, int total, bool? enablePagination = false)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalPages = (int)Math.Ceiling(total / (double)pageSize);
            EnablePagination = enablePagination;
        }
        public bool ShowStartRange => PageNumber - HalfMiddleBlockSize > 1 && TotalPages > MaxPageButtons;
        public bool ShowEndRange => PageNumber + HalfMiddleBlockSize < TotalPages && TotalPages > MaxPageButtons;
    }
}
