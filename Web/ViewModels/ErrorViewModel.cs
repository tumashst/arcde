﻿namespace Web.ViewModels
{
    public record ErrorViewModel(int Code, string Title, string Description);
}
