﻿using Auto.Interfaces;
using Auto.IO;
using Data.Repository;
using Data.Sql;
using MediatR;
using Model.Application;
using Web.Interfaces;
using Web.IO;
using Web.Mediator.Handlers;
using Web.Mediator.Requests;
using Web.Models.Security;

namespace Web;

public static class ServicesConfigurationExtension
{
    public static IServiceCollection AddRequestHandlers(this IServiceCollection services)
    {
        services.AddTransient<IRequestHandler<BuyRequest, Unit>, UserRequestHandler<BuyRequest>>();
        services.AddTransient<IRequestHandler<SellRequest, Unit>, UserRequestHandler<SellRequest>>();
        services.AddTransient<IRequestHandler<AddRequest, Unit>, UserRequestHandler<AddRequest>>();
        services.AddTransient<IRequestHandler<RemoveRequest, Unit>, UserRequestHandler<RemoveRequest>>();
        services.AddTransient<IRequestHandler<UpdateRequest, Unit>, UserRequestHandler<UpdateRequest>>();
        return services;
    }

    public static IServiceCollection AddSecurityServices(this IServiceCollection services)
    {
        services.AddScoped<IConfirmationTokenProvider, ConfirmationTokenProvider>();
        services.AddSingleton<IHasher, Hasher>();
        services.AddSingleton<ISaltProvider, SaltProvider>();
        services.AddSingleton<IPasswordHasher, PasswordHasher>();
        services.AddSingleton<IToBytesConverter, ToBytesConverter>();
        return services;
    }

    public static IServiceCollection AddDataServices(this IServiceCollection services)
    {
        services.AddDbContext<ApplicationContext>();
        services.AddSingleton<IFactory<string, IApplicationUnitOfWork>, UnitOfWorkFactory>();
        services.AddScoped<IRepository<User>, UserRepository>();
        services.AddScoped<IRepository<Role>, RolesRepository>();
        return services;
    }

    public static IServiceCollection AddUserServices(this IServiceCollection services)
    {
        services.AddScoped<IAuthenticationService, AuthenticationService>();
        services.AddScoped<IRegistrationService, RegistrationService>();
        services.AddScoped<IUserService, UserService>();
        return services;
    }

    public static IServiceCollection AddIOServices(this IServiceCollection services)
    {
        services.AddSingleton<Receiver>();
        services.AddSingleton<IIOProvider, WebIOProvider>();
        return services;
    }

    public static IServiceCollection AddEmailServices(this IServiceCollection services)
    {
        services.AddScoped<IEmailSender, EmailSender>();
        services.AddScoped<IConfirmationEmailSender, ConfirmationEmailSender>();
        services.AddScoped<IOrderEmailSender, OrderEmailSender>();
        return services;
    }
}