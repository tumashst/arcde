﻿using Model.Office;

namespace Web.IO;

public class Receiver
{
    public CarProduct[] Cars { get; set; }
    public Body[] Bodies { get; set; }
    public EngineType[] EngineTypes { get; set; }
    public Transmission[] Transmissions { get; set; }
    public DriveUnit[] DriveUnits { get; set; }

    public string[] Strings;
}