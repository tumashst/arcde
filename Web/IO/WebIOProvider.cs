﻿using Auto.IO;
using Model.Office;

namespace Web.IO;

public class WebIOProvider : IIOProvider
{
    private readonly Receiver _receiver;
    private readonly ILogger _logger;

    public WebIOProvider(Receiver receiver, ILogger logger)
    {
        _receiver = receiver;
        _logger = logger;
    }
    public void Write(CarProduct[] cars)
    {
        _receiver.Cars = cars;
    }

    public void Write(EngineType[] engineTypes)
    {
        _receiver.EngineTypes = engineTypes;
    }

    public void Write(Transmission[] transmissions)
    {
        _receiver.Transmissions = transmissions;
    }

    public void Write(Body[] bodies)
    {
        _receiver.Bodies = bodies;
    }

    public void Write(DriveUnit[] driveUnits)
    {
        _receiver.DriveUnits = driveUnits;
    }

    public string ReadString()
    {
        throw new NotImplementedException();
    }

    public void Write(string @string)
    {
        _logger.LogInformation(@string);
    }

    public void Write(string[] strings)
    {
        _receiver.Strings = strings;
    }
}