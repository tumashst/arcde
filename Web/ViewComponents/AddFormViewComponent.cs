﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.Mediator.Requests;
using Web.ViewModels;

namespace Web.ViewComponents
{
    public class AddFormViewComponent : ViewComponent
    {
        private readonly IMediator _mediator;

        public AddFormViewComponent(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<IViewComponentResult> InvokeAsync(
            string mark)
        {

            var bodies = await _mediator.Send(new GetBodiesRequest(new(mark)));

            var driveUnits = await _mediator.Send(new GetDriveUnitsRequest(new(mark)));

            var engineTypes = await _mediator.Send(new GetEngineTypesRequest(new(mark)));

            var transmissions = await _mediator.Send(new GetTransmissionsRequest(new(mark)));

            var viewModel = new AddFormViewModel(engineTypes, transmissions, bodies, driveUnits) { Mark = mark };

            return View(viewModel);
        }
    }
}
