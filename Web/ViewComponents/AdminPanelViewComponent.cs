﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.Mediator.Requests;
using Web.ViewModels;

namespace Web.ViewComponents
{
    public class AdminPanelViewComponent : ViewComponent
    {
        private readonly IMediator _mediator;

        public AdminPanelViewComponent(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<IViewComponentResult> InvokeAsync(
            string officeName)
        {
            var cars = await _mediator.Send(new GetCarsRequest(new(officeName)));

            var model = new AdminPanelViewModel(officeName, cars);

            return View(model);
        }

    }
}
