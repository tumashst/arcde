﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;
using Web.ViewModels;

namespace Web.ViewComponents
{
    public class EditFormViewComponent : ViewComponent
    {
        private readonly IMediator _mediator;

        public EditFormViewComponent(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<IViewComponentResult> InvokeAsync(
            string mark, string model)
        {

            var bodies = await _mediator.Send(new GetBodiesRequest(new(mark)));

            var driveUnits = await _mediator.Send(new GetDriveUnitsRequest(new(mark)));

            var engineTypes = await _mediator.Send(new GetEngineTypesRequest(new(mark)));

            var transmissions = await _mediator.Send(new GetTransmissionsRequest(new(mark)));

            var carProduct = await _mediator.Send(new GetCarRequest(new GetCarRequestArguments(mark, model)));


            var viewModel = new EditFormViewModel(engineTypes, transmissions, bodies, driveUnits)
            {
                Id = carProduct.Id,
                Model = carProduct.Subject.Model,
                Body = carProduct.Subject.Body,
                Year = carProduct.Subject.Year,
                Price = carProduct.Price,
                Color = carProduct.Subject.Color,
                Count = carProduct.Count,
                Description = carProduct.Description,
                DriveUnit = carProduct.Subject.DriveUnit,
                Engine = carProduct.Subject.Engine,
                Image = carProduct.Image,
                Mark = carProduct.Subject.Mark
            };

            return View(viewModel);
        }
    }
}
