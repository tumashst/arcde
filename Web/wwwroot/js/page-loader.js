﻿$(() => {
  changePage = (pageNumber, count, showPagination) => {
    if (showPagination) showLoader()
    $.get(`cars/catalog?page=${pageNumber}&count=${count}&enablePagination=${showPagination}`, (res) => {
      sessionStorage.setItem('page', pageNumber)
      $('#catalog').html(res)
      if (showPagination) {
        hideLoader()
        interceptPaging()
      }
    })
  }
})
