﻿const load_partial = (link, selector) =>
    fetch(link)
        .then(response => response.text())
        .then(body => {
            console.log(body);
            $(selector).html();
            $(selector).html(body);
            $(selector).children().first().modal('show');

        }
        )