﻿$(() => {
  showLoader = () => {
    $('#cars-cards-block').addClass('load-blur')
    $('#loader').show()
  }

  hideLoader = () => {
    $('#cars-cards-block').removeClass('load-blur')
    $('#loader').hide()
  }

  interceptPaging = () => {
    $('#pagination button').not('.active').click(function (event) {
      event.preventDefault()
      changePage($(this).attr('value'), 8, true)
    })
  }

  const page = sessionStorage.getItem('page')
  changePage((page || 1), 8, true)
})
