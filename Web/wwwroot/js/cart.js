﻿$(() => {
})

changeNumberValue = (counter, value) => {
    counter.val(parseInt(counter.val(), 10) + value)
}

inputEdit = (thisPrice, id, inputId) => {
    let counter = $('#' + inputId);
    switch (id) {
        case '+':
            counter.value++;
            changeNumberValue(counter, 1)
            totalPrice += thisPrice;
            break;
        case '-':
            console.log(counter.val());
            if (counter.val() <= 1) {
                return;
            }
            changeNumberValue(counter, -1)
            totalPrice -= thisPrice;
            break;
    }
    
    $("#total").html("$" + totalPrice);
}
