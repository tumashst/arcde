﻿using Web.Models.Security;

namespace Web;

public static class ConfigurationExtensions
{
    public static EmailConfiguration GetEmailConfiguration(this IConfiguration configuration)
        => configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>();

}