﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Web.Binders;

public class ImageToByteArrayBinder : IModelBinder
{
    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        if (bindingContext.ModelMetadata.ModelType == typeof(byte[]))
        {
            if (bindingContext.HttpContext.Request.Form.Files.Count > 0)
            {
                var file = bindingContext.HttpContext.Request.Form.Files[0];

                await using var memoryStream = new MemoryStream();

                await file.CopyToAsync(memoryStream);

                bindingContext.Result =
                    ModelBindingResult.Success(memoryStream.ToArray());
                return;
            }

            var modelName = bindingContext.ModelName;

            var modelNameValueProviderResult = bindingContext.ValueProvider.GetValue(modelName);

            if (modelNameValueProviderResult == ValueProviderResult.None)
            {
                return;
            }

            var base64image = modelNameValueProviderResult.Values.FirstOrDefault(val => val.Length > 0);

            if (base64image == "}")
            {
                bindingContext.Result = ModelBindingResult.Success(Array.Empty<byte>());
            }
            else
            {
                var bytes = Convert.FromBase64String(base64image);
                bindingContext.Result = ModelBindingResult.Success(bytes);
            }

        }

        else
        {
            bindingContext.Result = ModelBindingResult.Failed();
        }

    }
}