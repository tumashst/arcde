﻿using MediatR;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Model.Office;
using Web.Mediator.Requests;

namespace Web.Binders;

public class IdBinder : IModelBinder
{
    private readonly IMediator _mediator;

    public IdBinder(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        var modelName = bindingContext.ModelName;

        var idValueProviderResult = bindingContext.ValueProvider.GetValue($"{modelName}.Id");

        if (idValueProviderResult == ValueProviderResult.None)
        {
            return;
        }

        if (int.TryParse(idValueProviderResult.FirstValue, out int id))
        {
            var markValueProviderResult = bindingContext.ValueProvider.GetValue("Mark");

            if (markValueProviderResult == ValueProviderResult.None)
            {
                return;
            }

            var mark = markValueProviderResult.FirstValue;

            var modelType = bindingContext.ModelMetadata.ModelType;

            if (modelType == typeof(Body))
            {
                var bodies = await _mediator.Send(new GetBodiesRequest(new(mark)));
                bindingContext.Result = ModelBindingResult.Success(bodies.FirstOrDefault(body => body.Id == id));
                return;
            }
            if (modelType == typeof(Transmission))
            {
                var transmissions = await _mediator.Send(new GetTransmissionsRequest(new(mark)));
                bindingContext.Result = ModelBindingResult.Success(transmissions.FirstOrDefault(transmission => transmission.Id == id));
                return;
            }
            if (modelType == typeof(DriveUnit))
            {
                var driveUnits = await _mediator.Send(new GetDriveUnitsRequest(new(mark)));
                bindingContext.Result = ModelBindingResult.Success(driveUnits.FirstOrDefault(driveUnit => driveUnit.Id == id));
                return;
            }
            if (modelType == typeof(EngineType))
            {
                var engineTypes = await _mediator.Send(new GetEngineTypesRequest(new(mark)));
                bindingContext.Result = ModelBindingResult.Success(engineTypes.FirstOrDefault(engineType => engineType.Id == id));
                return;
            }
        }
        bindingContext.Result = ModelBindingResult.Failed();


    }
}