﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Model.Office;

namespace Web.Binders;

public class ColorStringToRgbColorBinder : IModelBinder
{
    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        if (bindingContext.ModelMetadata.ModelType == typeof(Color))
        {
            var modelName = bindingContext.ModelName;

            var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);

            if (valueProviderResult == ValueProviderResult.None)
            {
                return;
            }

            var value = valueProviderResult.FirstValue;

            var rPart = value[1..3];
            var gPart = value[3..5];
            var bPart = value[5..7];

            var color = new Color()
            {
                Red = Convert.ToInt32(rPart, 16),
                Green = Convert.ToInt32(gPart, 16),
                Blue = Convert.ToInt32(bPart, 16)
            };

            bindingContext.Result = ModelBindingResult.Success(color);

        }

        else
        {
            bindingContext.Result = ModelBindingResult.Failed();
        }

    }
}