﻿using MediatR;
using Web.Mediator.Arguments;
using Web.Models.Security;

namespace Web.Mediator.Requests;

public class LoginRequest : IRequest<AuthenticationResult>
{
    public string Email { get; }

    public string Password { get; }
    public LoginRequest(LoginRequestArguments arguments)
    {
        (Email, Password) = arguments;
    }
}