﻿using Auto.Request;
using MediatR;
using Model.Office;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class GetDriveUnitsRequest : UserRequest, IRequest<DriveUnit[]>
{
    public GetDriveUnitsRequest(GetDriveUnitsRequestArguments arguments) : base(UserRequestType.Get)
    {
        GetType = GetType.DriveUnits;
        Mark = arguments.Mark;
    }
}