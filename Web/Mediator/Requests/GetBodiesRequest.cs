﻿using Auto.Request;
using MediatR;
using Model.Office;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class GetBodiesRequest : UserRequest, IRequest<Body[]>
{
    public GetBodiesRequest(GetBodiesRequestArguments arguments) : base(UserRequestType.Get)
    {
        GetType = GetType.Bodies;
        Mark = arguments.Mark;
    }
}