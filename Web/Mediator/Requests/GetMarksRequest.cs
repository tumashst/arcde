﻿using Auto.Request;
using MediatR;

namespace Web.Mediator.Requests
{
    public class GetMarksRequest : UserRequest, IRequest<string[]>
    {
        public GetMarksRequest() : base(UserRequestType.Get)
        {
            GetType = GetType.Marks;
        }
    }
}
