﻿using MediatR;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests
{
    public class AddToCartRequest : IRequest
    {
        public BuyRequestArguments _arguments;
        public AddToCartRequest(BuyRequestArguments arguments)
        {
            _arguments = arguments;
        }
    }
}
