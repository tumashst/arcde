﻿using MediatR;

namespace Web.Mediator.Requests
{
    public class RemoveFromCartRequest : IRequest
    {
        public string _model;
        public string _mark;

        public RemoveFromCartRequest(string model, string mark)
        {
            _model = model;
            _mark = mark;
        }
    }
}
