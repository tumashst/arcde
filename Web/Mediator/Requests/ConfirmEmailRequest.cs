﻿using MediatR;
using Web.Mediator.Arguments;
using Web.Models.Security;

namespace Web.Mediator.Requests;

public class ConfirmEmailRequest : IRequest<EmailConfirmationResult>
{
    public string Email { get; }
    public string Token { get; }

    public ConfirmEmailRequest(ConfirmEmailRequestArguments arguments)
    {
        (Email, Token) = arguments;
    }
}