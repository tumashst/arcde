﻿using MediatR;
using Model.Office;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class GetCarsPageRequest : IRequest<CarProduct[]>
{
    public int PageNumber { get; }
    public int PageSize { get; }
    public GetCarsPageRequest(GetCarsPageRequestArguments arguments)
    {
        (PageNumber, PageSize) = arguments;
    }
}
