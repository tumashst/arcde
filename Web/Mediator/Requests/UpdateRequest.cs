﻿using Auto.Request;
using MediatR;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class UpdateRequest : UserRequest, IRequest
{
    public UpdateRequest(UpdateRequestArguments arguments) : base(UserRequestType.Update)
    {
        (Id, Mark, Model, Count, Price, Year,
            Description, Body, Engine, DriveUnit,
            Transmission, Color, Image) = arguments;
    }
}