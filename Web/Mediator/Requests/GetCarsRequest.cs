﻿using Auto.Request;
using MediatR;
using Model.Office;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class GetCarsRequest : UserRequest, IRequest<CarProduct[]>
{
    public GetCarsRequest(GetCarsRequestArguments arguments) : base(UserRequestType.Get)
    {
        GetType = GetType.Cars;
        Mark = arguments.Mark;
    }
}