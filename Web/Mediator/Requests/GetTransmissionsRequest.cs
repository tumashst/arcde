﻿using Auto.Request;
using MediatR;
using Model.Office;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class GetTransmissionsRequest : UserRequest, IRequest<Transmission[]>
{
    public GetTransmissionsRequest(GetTransmissionsRequestArguments arguments) : base(UserRequestType.Get)
    {
        GetType = GetType.Transmissions;
        Mark = arguments.Mark;
    }
}