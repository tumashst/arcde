﻿using MediatR;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class SendConfirmationEmailRequest : IRequest
{
    public string Email { get; }
    public SendConfirmationEmailRequest(SendConfirmationEmailRequestArguments arguments)
    {
        Email = arguments.Email;
    }
}