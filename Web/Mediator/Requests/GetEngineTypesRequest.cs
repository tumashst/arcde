﻿using Auto.Request;
using MediatR;
using Model.Office;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class GetEngineTypesRequest : UserRequest, IRequest<EngineType[]>
{
    public GetEngineTypesRequest(GetEngineTypesRequestArguments arguments) : base(UserRequestType.Get)
    {
        GetType = GetType.EngineTypes;
        Mark = arguments.Mark;
    }
}