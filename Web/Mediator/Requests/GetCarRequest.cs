﻿using MediatR;
using Model.Office;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests;

public class GetCarRequest : IRequest<CarProduct>
{
    public string Mark { get; }
    public string Model { get; }
    public GetCarRequest(GetCarRequestArguments arguments)
    {
        (Mark, Model) = arguments;
    }
}
