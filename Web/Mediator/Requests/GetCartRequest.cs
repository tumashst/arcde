﻿using MediatR;
using Web.Mediator.Arguments;

namespace Web.Mediator.Requests
{
    public class GetCartRequest : IRequest<List<BuyRequestArguments>>
    {
        public GetCartRequest()
        {

        }
    }
}
