﻿using MediatR;
using Web.Mediator.Arguments;
using Web.Models.Security;

namespace Web.Mediator.Requests;

public class RegisterRequest : IRequest<RegistrationResult>
{
    public string Email { get; }

    public string Password { get; }
    public RegisterRequest(RegisterRequestArguments arguments)
    {
        (Email, Password, _) = arguments;
    }
}