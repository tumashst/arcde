﻿namespace Web.Mediator.Arguments;

public record GetEngineTypesRequestArguments(string Mark = default);