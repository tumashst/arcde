﻿using System.ComponentModel.DataAnnotations;

namespace Web.Mediator.Arguments;

public class LoginRequestArguments
{
    [Required(ErrorMessage = "Email is required")]
    public string Email { get; set; }


    [Required(ErrorMessage = "Password is required")]
    public string Password { get; set; }

    public void Deconstruct(out string email, out string password) =>
       (email, password) = (Email, Password);

}