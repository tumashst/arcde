﻿namespace Web.Mediator.Arguments;

public record SendConfirmationEmailRequestArguments(string Email);