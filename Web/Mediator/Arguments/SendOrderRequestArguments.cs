﻿using MediatR;

namespace Web.Mediator.Arguments
{
    public class SendOrderRequestArguments : IRequest
    {
        public string Email { get; }
        public string Username { get; }
        public List<BuyRequestArguments> Items { get; }

        public SendOrderRequestArguments(string email, string username, List<BuyRequestArguments> items)
        {
            Email = email;
            Username = username;
            Items = items;
        }
    }
}
