﻿using System.ComponentModel.DataAnnotations;

namespace Web.Mediator.Arguments;

public class RegisterRequestArguments
{
    [Required(ErrorMessage = "Email is required")]
    public string Email { get; set; }

    [StringLength(maximumLength: 64, MinimumLength = 8, ErrorMessage = "Password range 8 - 64 characters")]
    [Compare(nameof(RepeatPassword), ErrorMessage = "Passwords must match")]
    [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).+$", ErrorMessage = "The password must contain: a-Z 0-9 and special characters")]
    public string Password { get; set; }

    [Required(ErrorMessage = "You need to repeat the password")]
    public string RepeatPassword { get; set; }

    public void Deconstruct(out string email, out string password, out string repeatPassword) =>
       (email, password, repeatPassword) = (Email, Password, RepeatPassword);

}