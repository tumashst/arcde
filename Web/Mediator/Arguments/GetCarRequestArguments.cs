﻿namespace Web.Mediator.Arguments;

public record GetCarRequestArguments(string Mark, string Model);