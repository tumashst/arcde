﻿namespace Web.Mediator.Arguments;

public record ConfirmEmailRequestArguments(string email, string token);
