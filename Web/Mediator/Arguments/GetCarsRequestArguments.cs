﻿namespace Web.Mediator.Arguments;

public record GetCarsRequestArguments(string Mark = default);