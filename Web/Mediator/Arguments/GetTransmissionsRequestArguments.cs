﻿namespace Web.Mediator.Arguments;

public record GetTransmissionsRequestArguments(string Mark = default);