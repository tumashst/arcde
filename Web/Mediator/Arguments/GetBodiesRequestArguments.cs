﻿namespace Web.Mediator.Arguments;

public record GetBodiesRequestArguments(string Mark = default);