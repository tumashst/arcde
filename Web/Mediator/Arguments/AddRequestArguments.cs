﻿using Microsoft.AspNetCore.Mvc;
using Model.Office;
using Web.Binders;

namespace Web.Mediator.Arguments;

public record AddRequestArguments(string Mark, string Model, int Count, int Price, int Year,
    string Description, [ModelBinder(typeof(IdBinder))] Body Body, Engine Engine, [ModelBinder(typeof(IdBinder))] DriveUnit DriveUnit,
    [ModelBinder(typeof(IdBinder))] Transmission Transmission, [ModelBinder(typeof(ColorStringToRgbColorBinder))] Color Color, [ModelBinder(typeof(ImageToByteArrayBinder))] byte[] Image);