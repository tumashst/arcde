﻿namespace Web.Mediator.Arguments;

public record GetDriveUnitsRequestArguments(string Mark = default);