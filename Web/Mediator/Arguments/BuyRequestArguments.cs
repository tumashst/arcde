﻿using System.ComponentModel.DataAnnotations;

namespace Web.Mediator.Arguments;

public class BuyRequestArguments
{
    public string Mark { get; set; }
    public string Model { get; set; }

    [DisplayFormat(DataFormatString = "${0:n0}")]
    public int Price { get; set; }
    public int Count { get; set; }

    public BuyRequestArguments()
    {
    }

    public BuyRequestArguments(string mark, string model, int price, int count)
    {
        Mark = mark;
        Model = model;
        Price = price;
        Count = count;
    }
    public void Deconstruct(out string mark, out string model, out int price, out int count) =>
    (mark, model, price, count) = (Mark, Model, Price, Count);
    public override string? ToString()
    {
        return $"{Mark} {Model} | {Count} | {Price}";
    }
}
