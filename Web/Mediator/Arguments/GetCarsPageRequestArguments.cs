﻿namespace Web.Mediator.Arguments;

public record GetCarsPageRequestArguments(int PageNumber, int PageSize = 12);