﻿using Microsoft.AspNetCore.Mvc;
using Model.Office;
using Web.Binders;

namespace Web.Mediator.Arguments;

public record UpdateRequestArguments(int Id, string Mark, string Model, int Count, int Price, int Year,
    string Description, Body Body, Engine Engine, DriveUnit DriveUnit,
    Transmission Transmission, [ModelBinder(typeof(ColorStringToRgbColorBinder))] Color Color, [ModelBinder(typeof(ImageToByteArrayBinder))] byte[] Image);