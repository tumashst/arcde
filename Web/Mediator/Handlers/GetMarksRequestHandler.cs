﻿using Auto.Interfaces;
using MediatR;
using Web.IO;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers
{
    public class GetMarksRequestHandler : IRequestHandler<GetMarksRequest, string[]>
    {
        private readonly IUserRequestProcessor _requestProcessor;
        private readonly ILogger _logger;

        public GetMarksRequestHandler(IUserRequestProcessor requestProcessor, ILogger logger)
        {
            _requestProcessor = requestProcessor;
            _logger = logger;
        }
        public Task<string[]> Handle(GetMarksRequest request, CancellationToken cancellationToken)
        {
            var receiver = new Receiver();

            request.IOProvider = new WebIOProvider(receiver, _logger);

            _requestProcessor.ProcessRequest(request);

            return Task.FromResult(receiver.Strings);
        }
    }
}
