﻿using Auto.Interfaces;
using MediatR;
using Model.Office;
using Web.IO;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers;

public class GetCarsRequestHandler : IRequestHandler<GetCarsRequest, CarProduct[]>
{
    private readonly IUserRequestProcessor _requestProcessor;
    private readonly ILogger _logger;

    public GetCarsRequestHandler(IUserRequestProcessor requestProcessor, ILogger logger)
    {
        _requestProcessor = requestProcessor;
        _logger = logger;
    }
    public Task<CarProduct[]> Handle(GetCarsRequest request, CancellationToken cancellationToken)
    {
        var receiver = new Receiver();

        request.IOProvider = new WebIOProvider(receiver, _logger);

        _requestProcessor.ProcessRequest(request);

        return Task.FromResult(receiver.Cars);
    }
}