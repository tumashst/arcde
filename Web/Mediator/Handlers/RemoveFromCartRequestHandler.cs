﻿using MediatR;
using System.Text.Json;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers
{
    public class RemoveFromCartRequestHandler : IRequestHandler<RemoveFromCartRequest>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public RemoveFromCartRequestHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<Unit> Handle(RemoveFromCartRequest request, CancellationToken cancellationToken)
        {
            var list = JsonSerializer.Deserialize<List<BuyRequestArguments>>(_httpContextAccessor.HttpContext.Request.Cookies["cart"]);
            list.Remove(list.First(i => i.Model == request._model && i.Mark == request._mark));
            _httpContextAccessor.HttpContext.Response.Cookies.Append("cart", JsonSerializer.Serialize(list));
            return Unit.Value;
        }
    }
}
