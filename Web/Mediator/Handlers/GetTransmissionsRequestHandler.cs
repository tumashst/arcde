﻿using Auto.Interfaces;
using MediatR;
using Model.Office;
using Web.IO;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers;

public class GetTransmissionsRequestHandler : IRequestHandler<GetTransmissionsRequest, Transmission[]>
{
    private readonly IUserRequestProcessor _requestProcessor;
    private readonly ILogger _logger;

    public GetTransmissionsRequestHandler(IUserRequestProcessor requestProcessor, ILogger logger)
    {
        _requestProcessor = requestProcessor;
        _logger = logger;
    }
    public Task<Transmission[]> Handle(GetTransmissionsRequest request, CancellationToken cancellationToken)
    {
        var receiver = new Receiver();

        request.IOProvider = new WebIOProvider(receiver, _logger);

        _requestProcessor.ProcessRequest(request);

        return Task.FromResult(receiver.Transmissions);
    }
}