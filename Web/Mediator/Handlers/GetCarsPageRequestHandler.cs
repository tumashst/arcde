﻿using MediatR;
using Model.Office;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers;

public class GetCarsPageRequestHandler : IRequestHandler<GetCarsPageRequest, CarProduct[]>
{
    private readonly IMediator _mediator;
    public GetCarsPageRequestHandler(IMediator mediator)
    {
        _mediator = mediator;
    }
    public async Task<CarProduct[]> Handle(GetCarsPageRequest request, CancellationToken cancellationToken)
    {
        var products = await _mediator.Send(new GetCarsRequest(new()));
        return products.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToArray();
    }
}