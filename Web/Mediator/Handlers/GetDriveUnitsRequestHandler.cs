﻿using Auto.Interfaces;
using MediatR;
using Model.Office;
using Web.IO;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers;

public class GetDriveUnitsRequestHandler : IRequestHandler<GetDriveUnitsRequest, DriveUnit[]>
{
    private readonly IUserRequestProcessor _requestProcessor;
    private readonly ILogger _logger;

    public GetDriveUnitsRequestHandler(IUserRequestProcessor requestProcessor, ILogger logger)
    {
        _requestProcessor = requestProcessor;
        _logger = logger;
    }
    public Task<DriveUnit[]> Handle(GetDriveUnitsRequest request, CancellationToken cancellationToken)
    {
        var receiver = new Receiver();

        request.IOProvider = new WebIOProvider(receiver, _logger);

        _requestProcessor.ProcessRequest(request);

        return Task.FromResult(receiver.DriveUnits);
    }
}