﻿using MediatR;
using System.Text.Json;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers
{
    public class AddToCartRequestHandler : IRequestHandler<AddToCartRequest>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AddToCartRequestHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<Unit> Handle(AddToCartRequest request, CancellationToken cancellationToken)
        {
            if (_httpContextAccessor.HttpContext.Request.Cookies.ContainsKey("cart"))
            {
                var list = JsonSerializer.Deserialize<List<BuyRequestArguments>>(_httpContextAccessor.HttpContext.Request.Cookies["cart"]);

                if (!list.Any(item => item.Model == request._arguments.Model))
                {
                    list.Add(request._arguments);
                }

                _httpContextAccessor.HttpContext.Response.Cookies.Append("cart", JsonSerializer.Serialize(list));
            }
            else
            {
                var list = new List<BuyRequestArguments>();
                list.Add(request._arguments);
                _httpContextAccessor.HttpContext.Response.Cookies.Append("cart", JsonSerializer.Serialize(list));
            }
            return Unit.Value;
        }
    }
}
