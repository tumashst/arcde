﻿using MediatR;
using System.Text.Json;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers
{
    public class GetCartRequestHandler : IRequestHandler<GetCartRequest, List<BuyRequestArguments>>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public GetCartRequestHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<List<BuyRequestArguments>> Handle(GetCartRequest request, CancellationToken cancellationToken)
        {
            return _httpContextAccessor.HttpContext.Request.Cookies.ContainsKey("cart") ? JsonSerializer.Deserialize<List<BuyRequestArguments>>(_httpContextAccessor.HttpContext.Request.Cookies["cart"]) : null;
        }
    }
}
