﻿using MediatR;
using Web.Interfaces;
using Web.Mediator.Arguments;

namespace Web.Mediator.Handlers
{
    public class SendOrderEmailRequestHandler : IRequestHandler<SendOrderRequestArguments>
    {
        private readonly IOrderEmailSender _confirmationEmailSender;

        public SendOrderEmailRequestHandler(IOrderEmailSender confirmationEmailSender)
        {
            _confirmationEmailSender = confirmationEmailSender;
        }

        public async Task<Unit> Handle(SendOrderRequestArguments request, CancellationToken cancellationToken)
        {
            await _confirmationEmailSender.SendOrderEmail(request.Email, request.Username, request.Items);
            return Task.FromResult(Unit.Value).Result;
        }
    }
}
