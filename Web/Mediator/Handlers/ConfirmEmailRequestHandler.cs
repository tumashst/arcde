﻿using MediatR;
using Web.Interfaces;
using Web.Mediator.Requests;
using Web.Models.Security;

namespace Web.Mediator.Handlers;

public class ConfirmEmailRequestHandler : IRequestHandler<ConfirmEmailRequest, EmailConfirmationResult>
{
    private readonly IUserService _userService;

    public ConfirmEmailRequestHandler(IUserService userService)
    {
        _userService = userService;
    }
    public Task<EmailConfirmationResult> Handle(ConfirmEmailRequest request, CancellationToken cancellationToken)
        => Task.FromResult(_userService.Confirm(request.Email, request.Token));
}