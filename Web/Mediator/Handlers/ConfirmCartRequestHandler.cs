﻿using MediatR;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers
{
    public class ConfirmCartRequestHandler : IRequestHandler<ConfirmCartRequest>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ConfirmCartRequestHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<Unit> Handle(ConfirmCartRequest request, CancellationToken cancellationToken)
        {
            _httpContextAccessor.HttpContext.Response.Cookies.Delete("cart");
            return Unit.Value;
        }
    }
}
