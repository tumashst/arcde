﻿using MediatR;
using Web.Interfaces;
using Web.Mediator.Requests;
using Web.Models.Security;

namespace Web.Mediator.Handlers;

public class LoginRequestHandler : IRequestHandler<LoginRequest, AuthenticationResult>
{
    private readonly IAuthenticationService _authenticationService;

    public LoginRequestHandler(IAuthenticationService authenticationService)
    {
        _authenticationService = authenticationService;
    }
    public Task<AuthenticationResult> Handle(LoginRequest request, CancellationToken cancellationToken)
    {

        return Task.FromResult(_authenticationService.Authenticate(request.Email, request.Password));
    }
}