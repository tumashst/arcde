﻿using Auto.Interfaces;
using Auto.Request;
using MediatR;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers;



public class UserRequestHandler<T> : IRequestHandler<T> where T : UserRequest, IRequest
{
    private readonly IUserRequestProcessor _requestProcessor;

    public UserRequestHandler(IUserRequestProcessor requestProcessor)
    {
        _requestProcessor = requestProcessor;
    }
    public Task<Unit> Handle(T request, CancellationToken cancellationToken)
    {
        _requestProcessor.ProcessRequest(request);
        return Task.FromResult(Unit.Value);
    }
}

public class AddRequestHandler : IRequestHandler<AddRequest>
{
    private readonly IUserRequestProcessor _requestProcessor;

    public AddRequestHandler(IUserRequestProcessor requestProcessor)
    {
        _requestProcessor = requestProcessor;
    }

    public Task<Unit> Handle(AddRequest request, CancellationToken cancellationToken)
    {

        _requestProcessor.ProcessRequest(request);
        return Task.FromResult(Unit.Value);
    }
}