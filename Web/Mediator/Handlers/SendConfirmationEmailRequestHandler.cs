﻿using MediatR;
using Web.Interfaces;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers;

public class SendConfirmationEmailRequestHandler : IRequestHandler<SendConfirmationEmailRequest>
{
    private readonly IConfirmationEmailSender _confirmationEmailSender;

    public SendConfirmationEmailRequestHandler(IConfirmationEmailSender confirmationEmailSender)
    {
        _confirmationEmailSender = confirmationEmailSender;
    }

    public async Task<Unit> Handle(SendConfirmationEmailRequest request, CancellationToken cancellationToken)
    {
        await _confirmationEmailSender.SendConfirmationEmail(request.Email);
        return Task.FromResult(Unit.Value).Result;
    }
}