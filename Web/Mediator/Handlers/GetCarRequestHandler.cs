﻿using MediatR;
using Model.Office;
using Web.Mediator.Requests;

namespace Web.Mediator.Handlers;

public class GetCarRequestHandler : IRequestHandler<GetCarRequest, CarProduct>
{
    private readonly IMediator _mediator;

    public GetCarRequestHandler(IMediator mediator)
    {
        _mediator = mediator;
    }
    public async Task<CarProduct> Handle(GetCarRequest request, CancellationToken cancellationToken)
    {
        var products = await _mediator.Send(new GetCarsRequest(new(request.Mark)));
        return products.FirstOrDefault(product => product.Subject.Model == request.Model);
    }
}