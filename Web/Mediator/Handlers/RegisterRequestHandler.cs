﻿using MediatR;
using Web.Interfaces;
using Web.Mediator.Requests;
using Web.Models.Security;

namespace Web.Mediator.Handlers;

public class RegisterRequestHandler : IRequestHandler<RegisterRequest, RegistrationResult>
{
    private readonly IRegistrationService _registrationService;

    public RegisterRequestHandler(IRegistrationService registrationService)
    {
        _registrationService = registrationService;
    }
    public Task<RegistrationResult> Handle(RegisterRequest request, CancellationToken cancellationToken)
    {

        return Task.FromResult(_registrationService.Register(request.Email, request.Password));
    }
}