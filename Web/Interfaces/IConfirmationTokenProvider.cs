﻿namespace Web.Interfaces;

public interface IConfirmationTokenProvider
{
    string GetConfirmationToken();
}