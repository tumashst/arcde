﻿using Model.Application;
using Web.Models.Security;

namespace Web.Interfaces;

public interface IUserService
{
    User? GetByEmail(string email);

    void Add(User user);

    bool IsConfirmed(string email);
    EmailConfirmationResult Confirm(string email, string confirmationToken);
}