﻿namespace Web.Interfaces;

public interface ISaltProvider
{
    byte[] GetSalt();
}