﻿namespace Web.Interfaces;

public interface IToBytesConverter
{
    byte[] GetBytes(string value);
}