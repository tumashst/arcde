﻿namespace Web.Interfaces;

public interface IHasher
{
    byte[] Hash(byte[] value);
}
