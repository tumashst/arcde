﻿using Model.Application;

namespace Web.Interfaces;

public interface IPasswordHasher
{
    PasswordHash Hash(string password);
}