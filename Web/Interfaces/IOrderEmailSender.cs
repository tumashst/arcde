﻿namespace Web.Interfaces
{
    public interface IOrderEmailSender
    {
        public Task SendOrderEmail(string email, string username, IEnumerable<object> content);
    }
}
