﻿namespace Web.Interfaces;

public interface IConfirmationEmailSender
{
    public Task SendConfirmationEmail(string email);
}