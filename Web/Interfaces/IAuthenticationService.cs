﻿using Web.Models.Security;

namespace Web.Interfaces;

public interface IAuthenticationService
{
    public AuthenticationResult Authenticate(string email, string password);

}