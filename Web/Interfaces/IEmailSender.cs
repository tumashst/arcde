﻿using Web.Models.Security;

namespace Web.Interfaces;

public interface IEmailSender
{
    void SendEmail(Message message);
}