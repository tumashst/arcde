﻿using Web.Models.Security;

namespace Web.Interfaces;

public interface IRegistrationService
{

    RegistrationResult Register(string email, string password);
}