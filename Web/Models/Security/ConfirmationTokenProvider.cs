﻿using Web.Interfaces;

namespace Web.Models.Security;

public class ConfirmationTokenProvider : IConfirmationTokenProvider
{
    public string GetConfirmationToken() => Guid.NewGuid().ToString()[..8];
}