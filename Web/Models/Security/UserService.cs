﻿using Auto.Interfaces;
using Model.Application;
using Web.Interfaces;

namespace Web.Models.Security;

public class UserService : IUserService
{
    private readonly IRepository<User> _userRepository;

    public UserService(IRepository<User> userRepository) => _userRepository = userRepository;

    public User? GetByEmail(string email)
        => _userRepository.All().FirstOrDefault(user => user.Email == email);

    public void Add(User user) => _userRepository.Add(user);

    public bool IsConfirmed(string email)
        => GetByEmail(email)?.IsConfirmed ?? false;

    public EmailConfirmationResult Confirm(string email, string confirmationToken)
    {
        if (GetByEmail(email) is { } user)
        {
            if (!user.IsConfirmed)
            {
                if (user.ConfirmationToken == confirmationToken)
                {
                    user.IsConfirmed = true;
                    _userRepository.Update(user);
                    return EmailConfirmationResult.Success(user);
                }
                return EmailConfirmationResult.Failure(EmailConfirmationFailureReason.InvalidToken);
            }
            return EmailConfirmationResult.Failure(EmailConfirmationFailureReason.AlreadyConfirmed);
        }
        return EmailConfirmationResult.Failure(EmailConfirmationFailureReason.InvalidEmail);
    }
}