﻿using Model.Application;

namespace Web.Models.Security;

public class EmailConfirmationResult
{
    private EmailConfirmationResult(User user, bool isSuccessful, EmailConfirmationFailureReason? registrationFailureReason = null)
        => (User, IsSuccessful, FailureReason) = (user, isSuccessful, registrationFailureReason);
    public bool IsSuccessful { get; }

    public EmailConfirmationFailureReason? FailureReason { get; }

    public User? User;
    public static EmailConfirmationResult Success(User user) => new(user, true);
    public static EmailConfirmationResult Failure(EmailConfirmationFailureReason reason) => new(null, false, reason);

}