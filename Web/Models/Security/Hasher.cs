﻿using System.Security.Cryptography;
using Web.Interfaces;

namespace Web.Models.Security;

public class Hasher : IHasher
{
    private static readonly SHA1 HasherInstance = SHA1.Create();

    public byte[] Hash(byte[] value)
        => HasherInstance.ComputeHash(value);

}