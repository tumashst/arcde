﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using RazorHtmlEmails.RazorClassLib.Services;
using Web.Interfaces;

namespace Web.Models.Security;

public class OrderEmailSender : IOrderEmailSender
{
    private readonly IActionContextAccessor _actionContextAccessor;
    private readonly IEmailSender _emailSender;
    private readonly IUserService _userService;
    private readonly IUrlHelperFactory _urlHelperFactory;
    private readonly IRazorViewToStringRenderer _renderer;

    public OrderEmailSender(IActionContextAccessor actionContextAccessor, IEmailSender emailSender, IUserService userService, IUrlHelperFactory urlHelperFactory, IRazorViewToStringRenderer renderer)
    {
        _actionContextAccessor = actionContextAccessor;
        _emailSender = emailSender;
        _userService = userService;
        _urlHelperFactory = urlHelperFactory;
        _renderer = renderer;
    }

    public async Task SendOrderEmail(string email, string username, IEnumerable<object> products)
    {
        var body = await _renderer.RenderViewToStringAsync("/Views/Mail/OrdersForAdmin.cshtml", products);
        _emailSender.SendEmail(new Message(email, $"User {username} ordered", body));
    }
}