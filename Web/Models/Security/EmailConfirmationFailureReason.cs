﻿namespace Web.Models.Security;

public enum EmailConfirmationFailureReason
{
    InvalidToken, InvalidEmail, AlreadyConfirmed
}