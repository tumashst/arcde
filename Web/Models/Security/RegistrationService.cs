﻿using Auto.Interfaces;
using Model.Application;
using Web.Interfaces;

namespace Web.Models.Security;

class RegistrationService : IRegistrationService
{
    private readonly IUserService _userService;
    private readonly IConfirmationTokenProvider _confirmationTokenProvider;
    private readonly IPasswordHasher _passwordHasher;
    private readonly IRepository<Role> _roleRepository;

    public RegistrationService(IUserService userService, IConfirmationTokenProvider confirmationTokenProvider,
        IPasswordHasher passwordHasher, IRepository<Role> roleRepository)
    {
        _userService = userService;
        _confirmationTokenProvider = confirmationTokenProvider;
        _passwordHasher = passwordHasher;
        _roleRepository = roleRepository;
    }

    public RegistrationResult Register(string email, string password)
    {
        if (_userService.GetByEmail(email) is null)
        {
            var user = new User
            {
                Email = email,
                PasswordHash = _passwordHasher.Hash(password),
                Role = _roleRepository.All().First(role => role.Name == nameof(RoleName.User)),
                ConfirmationToken = _confirmationTokenProvider.GetConfirmationToken(),
                IsConfirmed = false
            };

            _userService.Add(user);

            return RegistrationResult.Success(user);
        }

        return RegistrationResult.Failure(RegistrationFailureReason.EmailAlreadyExists);
    }
}