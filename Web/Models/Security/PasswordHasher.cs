﻿using Model.Application;
using Web.Interfaces;

namespace Web.Models.Security
{
    public class PasswordHasher : IPasswordHasher
    {
        private readonly IHasher _hasher;
        private readonly ISaltProvider _saltProvider;
        private readonly IToBytesConverter _toBytesConverter;

        public PasswordHasher(IHasher hasher, ISaltProvider saltProvider, IToBytesConverter toBytesConverter)
        {
            _hasher = hasher;
            _saltProvider = saltProvider;
            _toBytesConverter = toBytesConverter;
        }

        public PasswordHash Hash(string password)
        {
            var passwordBytes = _toBytesConverter.GetBytes(password);
            var salt = _saltProvider.GetSalt();
            var hashedPassword = _hasher.Hash(passwordBytes, salt);
            return new()
            {
                HashValue = hashedPassword,
                Salt = salt
            };
        }
    }
}
