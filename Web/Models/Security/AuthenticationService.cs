﻿using Web.Interfaces;

namespace Web.Models.Security;

public class AuthenticationService : IAuthenticationService
{
    private readonly IUserService _userService;
    private readonly IHasher _hasher;
    private readonly IToBytesConverter _converter;

    public AuthenticationService(IUserService userService, IHasher hasher, IToBytesConverter converter)
    {
        _userService = userService;
        _hasher = hasher;
        _converter = converter;
    }

    public AuthenticationResult Authenticate(string email, string password)
    {

        var user = _userService.GetByEmail(email);

        if (user is not null)
        {
            var encodedPassword = _converter.GetBytes(password);

            var salt = user.PasswordHash.Salt;

            var hashedPassword = _hasher.Hash(encodedPassword, salt);

            if (hashedPassword.SequenceEqual(user.PasswordHash.HashValue))
            {
                return AuthenticationResult.Success(user);
            }
        }
        return AuthenticationResult.Failure();
    }
}