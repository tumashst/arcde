﻿using System.Text;
using Web.Interfaces;

namespace Web.Models.Security;

static class HasherExtensions
{
    public static byte[] Hash(this IHasher hasher, string value)
        => hasher.Hash(Encoding.UTF8.GetBytes(value));

    public static byte[] Hash(this IHasher hasher, byte[] value, byte[] salt)
        => hasher.Hash(value.Concat(salt).ToArray());

}