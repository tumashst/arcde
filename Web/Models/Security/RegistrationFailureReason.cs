﻿namespace Web.Models.Security;

public enum RegistrationFailureReason
{
    EmailAlreadyExists
}