﻿using System.Security.Cryptography;
using Web.Interfaces;

namespace Web.Models.Security;

class SaltProvider : ISaltProvider
{
    private static readonly int Length = 23;

    public byte[] GetSalt()
        => RandomNumberGenerator.GetBytes(Length);
}