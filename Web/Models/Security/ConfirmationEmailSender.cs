﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using RazorHtmlEmails.RazorClassLib.Services;
using Web.Interfaces;

namespace Web.Models.Security;

public class ConfirmationEmailSender : IConfirmationEmailSender
{
    private readonly IActionContextAccessor _actionContextAccessor;
    private readonly IEmailSender _emailSender;
    private readonly IUserService _userService;
    private readonly IUrlHelperFactory _urlHelperFactory;
    private readonly IRazorViewToStringRenderer _renderer;

    public ConfirmationEmailSender(IActionContextAccessor actionContextAccessor, IEmailSender emailSender, IUserService userService, IUrlHelperFactory urlHelperFactory, IRazorViewToStringRenderer renderer)
    {
        _actionContextAccessor = actionContextAccessor;
        _emailSender = emailSender;
        _userService = userService;
        _urlHelperFactory = urlHelperFactory;
        _renderer = renderer;

    }
    public async Task SendConfirmationEmail(string email)
    {
        var token = _userService.GetByEmail(email)?.ConfirmationToken ??
                    throw new ArgumentNullException(nameof(email));

        var context = _actionContextAccessor.ActionContext;

        var urlHelper = _urlHelperFactory.GetUrlHelper(context);

        var link = urlHelper.Action(new UrlActionContext()
        { Action = "Confirm", Controller = "Account", Values = new { email, token }, Protocol = context.HttpContext.Request.Scheme });

        var body = await _renderer.RenderViewToStringAsync("/Views/Mail/ConfirmEmail.cshtml", link);
        _emailSender.SendEmail(new Message(email, $"Email confirmation", body));
    }
}