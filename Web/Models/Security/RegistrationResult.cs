﻿using Model.Application;

namespace Web.Models.Security;

public class RegistrationResult
{
    private RegistrationResult(User user, bool isSuccessful, RegistrationFailureReason? registrationFailureReason = null)
        => (User, IsSuccessful, FailureReason) = (user, isSuccessful, registrationFailureReason);
    public bool IsSuccessful { get; }

    public RegistrationFailureReason? FailureReason { get; }

    public User? User;
    public static RegistrationResult Success(User user) => new(user, true);
    public static RegistrationResult Failure(RegistrationFailureReason reason) => new(null, false, reason);

}