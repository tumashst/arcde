﻿using Model.Application;

namespace Web.Models.Security;

public class AuthenticationResult
{
    private AuthenticationResult(User user, bool isSuccessful) => (User, IsSuccessful) = (user, isSuccessful);
    public bool IsSuccessful { get; }

    public User? User;
    public static AuthenticationResult Success(User user) => new(user, true);

    public static AuthenticationResult Failure() => new(null, false);
}