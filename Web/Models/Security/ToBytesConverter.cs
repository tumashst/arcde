﻿using System.Text;
using Web.Interfaces;

namespace Web.Models.Security;

public class ToBytesConverter : IToBytesConverter
{
    public byte[] GetBytes(string value)
        => Encoding.UTF8.GetBytes(value);

}