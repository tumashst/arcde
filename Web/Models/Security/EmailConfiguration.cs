﻿namespace Web.Models.Security;

public class EmailConfiguration
{
    public EmailConfiguration()
    {
    }

    public string From { get; init; }
    public string SmtpServer { get; init; }
    public int Port { get; init; }
    public string Login { get; init; }
    public string Password { get; init; }

}