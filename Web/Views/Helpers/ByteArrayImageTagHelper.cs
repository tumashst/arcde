﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Web.Views
{
    [HtmlTargetElement("img", Attributes = "asp-byte-image")]
    public class ByteArrayImageTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-byte-image")]
        public byte[]? ImageData
        {
            get;
            set;
        }

        [HtmlAttributeName("asp-fallback-src")]
        public string? FallbackSource
        {
            get;
            set;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (ImageData is not null and not { Length: 0 })
            {
                output.Attributes.SetAttribute("src", $"data:image;base64,{Convert.ToBase64String(ImageData)}");
            }
            else if (FallbackSource is not null)
            {
                output.Attributes.SetAttribute("src", FallbackSource);
            }
        }

    }
}
