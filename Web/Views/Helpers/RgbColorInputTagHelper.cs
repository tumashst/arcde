﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using Model.Office;

namespace Web.Views
{
    [HtmlTargetElement("input", Attributes = "asp-rgb-color")]
    public class RgbColorInputTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-rgb-color")]
        public Color? Color
        {
            get;
            set;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (Color is not null)
            {
                string hexColor = $"#{Color.Red:x2}{Color.Green:x2}{Color.Blue:x2}";

                output.Attributes.SetAttribute("value", hexColor);
            }

        }

    }
}
