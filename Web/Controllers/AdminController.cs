﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;
using Web.ViewModels;

namespace Web.Controllers;

[Authorize(Roles = "Admin")]
[Route("[controller]")]
public class AdminController : Controller
{

    private readonly IMediator _mediator;

    public AdminController(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task<IActionResult> Index()
    {
        var marks = await _mediator.Send(new GetMarksRequest());

        var carProducts = await _mediator.Send(new GetCarsRequest(new()));

        var bodies = await _mediator.Send(new GetBodiesRequest(new()));

        var driveUnits = await _mediator.Send(new GetDriveUnitsRequest(new()));

        var engineTypes = await _mediator.Send(new GetEngineTypesRequest(new()));

        var transmissions = await _mediator.Send(new GetTransmissionsRequest(new()));

        return View(new AdminViewModel(marks, carProducts, driveUnits, transmissions, engineTypes, bodies));
    }

    [HttpDelete]
    public IActionResult Delete(string mark, string model)
    {
        _mediator.Send(new RemoveRequest(new RemoveRequestArguments(mark, model)));

        return Ok();
    }

    [HttpPost("add")]
    public IActionResult Add([FromForm] AddRequestArguments arguments)
    {
        _mediator.Send(new AddRequest(arguments));

        return RedirectToAction("Index");
    }

    [HttpPost("edit")]
    public IActionResult Edit([FromForm] UpdateRequestArguments arguments)
    {
        _mediator.Send(new UpdateRequest(arguments));
        return RedirectToAction("Index");
    }

    [HttpGet("edit")]
    public IActionResult Edit(string mark, string model)
    {

        return ViewComponent("EditForm", new { mark, model });
    }

    [HttpGet("add")]
    public IActionResult Add(string mark)
    {

        return ViewComponent("AddForm", new { mark });
    }
}