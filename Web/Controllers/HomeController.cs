﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers;

[Route("/")]
[Route("/Home")]
public class HomeController : Controller
{
    public IActionResult Index()
    {
        return View();
    }
}