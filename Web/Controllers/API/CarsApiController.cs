﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Model.Office;
using Web.Mediator.Requests;

namespace Web.Controllers.API;

[Route("api/cars")]
public class CarsApiController : ControllerBase
{
    private readonly IMediator _mediator;

    public CarsApiController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet()]
    public async Task<IActionResult> GetPage(int page)
    {
        var carProducts = await _mediator.Send(new GetCarsPageRequest(new(page, 12)));
        return Ok(carProducts);
    }

    [HttpGet("{mark}/models")]
    public async Task<ActionResult<string[]>> GetModels(string mark)
    {
        var models = await _mediator.Send(new GetModelsRequest(new(mark)));
        return models;
    }

    [HttpGet("{mark}/{model}")]
    public async Task<ActionResult<CarProduct>> Get(string mark, string model)
    {
        var car = await _mediator.Send(new GetCarRequest(new(mark, model)));
        return car;
    }
}