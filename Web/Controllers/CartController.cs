﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.Office;
using System.Security.Claims;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;
using Web.ViewModels;

namespace Web.Controllers
{
    [Authorize(Roles = "User")]
    public class CartController : Controller
    {
        private readonly IMediator _mediator;
        public CartController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [Route("Cart")]
        [Route("Cart/Index")]
        public async Task<IActionResult> Index()
        {
            var list = await _mediator.Send(new GetCartRequest());

            int price = 0;
            var products = new List<Product<Car>>();

            if (list == null || list.Count == 0)
            {
                return View("EmptyCart");
            }
            foreach (var car in list)
            {
                var product = await _mediator.Send(new GetCarRequest(new(car.Mark, car.Model)));
                product.Count = 1;
                products.Add(product);
                price += product.Price;
            }

            return View(new CartViewModel(products, price, products.Count));
        }


        [HttpPost("Cart/Add")]
        public async Task<IActionResult> Add([FromForm] BuyRequestArguments arguments)
        {
            await _mediator.Send(new AddToCartRequest(arguments));
            return RedirectToAction("Index", "Cart");
        }

        [Route("Cart/Update")]
        public async Task<IActionResult> Delete(string model, string mark, int value)
        {
            await _mediator.Send(new RemoveFromCartRequest(model, mark));
            return Ok();
        }

        [Route("Cart/Delete")]
        public async Task<IActionResult> Delete(string model, string mark)
        {
            await _mediator.Send(new RemoveFromCartRequest(model, mark));
            return RedirectToAction("Index");
        }

        [HttpPost("Cart/Confirm")]
        public async Task<IActionResult> Confirm()
        {
            var products = await _mediator.Send(new GetCartRequest());
            if (products != null && products.Count > 0)
            {
                await _mediator.Send(new ConfirmCartRequest());
                await _mediator.Send(new SendOrderRequestArguments("breathtakingteam@gmail.com", User.FindFirst(ClaimTypes.Email)?.Value, products));

                return View("ConfirmResult", true);

            }
            return View("ConfirmResult", false);
        }
    }
}
