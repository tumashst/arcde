﻿using Microsoft.AspNetCore.Mvc;
using Shared;

namespace Web.Controllers
{
    [Route("[controller]")]
    public class ShopsController : Controller
    {
        [HttpGet()]
        public IActionResult Index([FromServices] IEnumerable<StorageConfiguration> config)
        {
            return View(config);
        }
    }
}
