﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("/About")]
    public class AboutController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
