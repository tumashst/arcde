﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;
using Web.ViewModels;

namespace Web.Controllers;

[Route("[controller]")]
public class CarsController : Controller
{
    private readonly IMediator _mediator;
    private const int MaxPageSize = 12;

    public CarsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet()]
    public IActionResult Index()
    {
        return View();
    }

    [HttpGet("catalog")]
    public async Task<IActionResult> GetPage(int page, int count, bool? enablePagination)
    {
        var pageProducts = await _mediator.Send(new GetCarsPageRequest(new(page, count)));
        var totalProducts = await _mediator.Send(new GetCarsRequest(new()));
        return PartialView("Partials/_CarsCatalog", new IndexViewModel(pageProducts, new PageViewModel(page, count, totalProducts.Length, enablePagination)));
    }

    [HttpGet("{mark}/{model}")]
    public async Task<IActionResult> CarDetails(string mark, string model)
    {
        var carProduct = await _mediator.Send(new GetCarRequest(new(mark, model)));
        return View(carProduct);
    }

    [HttpPost("sell")]
    public async Task<IActionResult> Sell([FromForm] SellRequestArguments arguments)
    {
        await _mediator.Send(new SellRequest(arguments));
        return RedirectToAction("Index");
    }
}