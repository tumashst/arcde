﻿using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.Application;
using System.Security.Claims;
using Web.Mediator.Arguments;
using Web.Mediator.Requests;
using Web.Models.Security;

namespace Web.Controllers;

public class AccountController : Controller
{
    private readonly IMediator _mediator;

    public AccountController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [Route("Register")]
    [HttpGet]
    public IActionResult Register()
    {
        if (User.Identity.IsAuthenticated)
        {
            return RedirectToAction("Index", "Home");
        }
        return View();
    }

    [Route("Confirm")]
    [HttpGet]
    public async Task<IActionResult> Confirm(ConfirmEmailRequestArguments arguments)
    {
        var response = await _mediator.Send(new ConfirmEmailRequest(arguments));
        if (response.IsSuccessful)
        {
            SignIn(response.User);
        }
        return View(nameof(EmailConfirmationResult), response);
    }

    [Route("Register")]
    [HttpPost]
    public async Task<IActionResult> Register([FromForm] RegisterRequestArguments arguments)
    {
        if (ModelState.IsValid)
        {
            var response = await _mediator.Send(new RegisterRequest(arguments));

            if (response.IsSuccessful)
            {
                try
                {
                    await _mediator.Send(new SendConfirmationEmailRequest(new(response.User.Email)));
                }
                catch
                {
                }
            }

            return View(nameof(RegistrationResult), response);
        }
        else return View();
    }

    [HttpGet]
    [Route("Login")]
    public IActionResult Login()
    {
        if (User.Identity.IsAuthenticated)
        {
            return RedirectToAction("Index", "Home");
        }
        return View();
    }

    [HttpPost]
    [Route("Login")]
    public async Task<IActionResult> Login([FromForm] LoginRequestArguments arguments)
    {
        if (ModelState.IsValid)
        {
            var response = await _mediator.Send(new LoginRequest(arguments));

            if (response.IsSuccessful)
            {
                if (!response.User.IsConfirmed)
                {
                    return View("ConfirmEmail");
                }
                await SignIn(response.User);

                return RedirectToAction("Index", "Cars");
            }
        }
        return View();
    }

    [Authorize]
    [HttpPost]
    [Route("Logout")]
    public async Task<IActionResult> Logout()
    {
        await HttpContext.SignOutAsync();
        return RedirectToAction("Index", "Home");
    }

    private async Task SignIn(User user)
    {

        var claims = new Claim[]
        {
            new (ClaimTypes.Email, user.Email),
            new (ClaimTypes.Role, user.Role.Name)
        };

        var identity = new ClaimsIdentity(claims, "ApplicationCookie", ClaimTypes.Email, ClaimTypes.Role);

        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));
    }

}