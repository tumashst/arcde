﻿using Microsoft.AspNetCore.Mvc;
using Web.ViewModels;

namespace Web.Controllers
{
    public class ErrorController : Controller
    {
        [Route("[controller]")]
        public IActionResult Index(int code)
        {
            return code switch
            {
                StatusCodes.Status401Unauthorized => View(new ErrorViewModel(code, "No authorization found", "This page is not publicaly available. To access it please login first.")),
                StatusCodes.Status403Forbidden => View(new ErrorViewModel(code, "Access denied", "You don't have permission to access requested page.")),
                StatusCodes.Status404NotFound => View(new ErrorViewModel(code, "Page not found", "The page you are looking for might have been removed had its name changed or is temporarily unavailable.")),
                StatusCodes.Status418ImATeapot => View(new ErrorViewModel(code, "ImATeapot", "")),
                StatusCodes.Status429TooManyRequests => View(new ErrorViewModel(code, "Slow down", "Please wait a few minutes before you try again.")),
                StatusCodes.Status500InternalServerError => View(new ErrorViewModel(code, "Server error", "")),
                _ => View(new ErrorViewModel(code, "Something wrong...", "")),
            };
        }
    }
}
