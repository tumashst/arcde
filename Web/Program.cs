using Auto;
using Auto.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using RazorHtmlEmails.RazorClassLib.Services;
using RequestProcessing.RequestProcessor;
using Shared;
using System.Reflection;
using Web;


var builder = WebApplication.CreateBuilder(args);

builder.Logging.ClearProviders();
builder.Logging.AddConsole();

builder.Services.AddSingleton<ILogger>(provider => provider.GetRequiredService<ILogger<Unit>>());

builder.Services.AddSingleton<HeadOffice>();
builder.Services.AddSingleton<IUserRequestProcessor, HeadOfficeUserRequestProcessor>();

builder.Services.AddMediatR(Assembly.GetExecutingAssembly());
builder.Services.AddRequestHandlers();

builder.Services.AddIOServices();

builder.Services.AddSecurityServices();
builder.Services.AddUserServices();

builder.Services.AddEmailServices();
var emailConfig = builder.Configuration.GetEmailConfiguration();
builder.Services.AddSingleton(emailConfig);

builder.Services.AddDataServices();
builder.Services.AddSingleton(builder.Configuration.GetStorageConfigurations());

builder.Services.AddScoped<IRazorViewToStringRenderer, RazorViewToStringRenderer>();
builder.Services.AddSingleton<IUrlHelperFactory, UrlHelperFactory>();
builder.Services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
builder.Services.AddHttpContextAccessor();

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages().AddRazorRuntimeCompilation();

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.LoginPath = new PathString("/Login");
        options.AccessDeniedPath = new PathString("/Login");
    });


var app = builder.Build();

app.MapControllers();

app.UseRouting();
app.UseStatusCodePagesWithReExecute("/error", "?code={0}");

app.UseAuthentication();
app.UseAuthorization();

app.UseStaticFiles();

app.Run();

