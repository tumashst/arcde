[![logo.png](https://i.postimg.cc/yNRCnYPp/logo.png)](https://postimg.cc/p9PSLv3K)
***
**AutoShop** is a web application that processes customer orders for the purchase of a car, and also provides tools for the administration of stores.
## Home page
***
[![main.png](https://i.postimg.cc/CxDrW7Ph/main.png)](https://postimg.cc/JsrQXNCv)
> Home page with new products and the best offers.
## Cars catalog
***
[![cars.png](https://i.postimg.cc/rmLctgsz/cars.png)](https://postimg.cc/wtFnCXhd)
> In the catalog, the customer can find brief information about cars or go to the details page. Or just add the car to the cart.
## Mobile version
***
Register / Login |  Home  | Details page
------------ | ------------- | ------------- 
 [![register-vertical.png](https://i.postimg.cc/RV65Pcwd/register-vertical.png)](https://postimg.cc/ykHbx3Q3) | [![about-vertical.png](https://i.postimg.cc/N0RM93d3/about-vertical.png)](https://postimg.cc/GBhr65wK) | [![details-vertical.png](https://i.postimg.cc/TPSncvxC/details-vertical.png)](https://postimg.cc/p9f927pF)

> All pages of the application have an adaptive design.


## Admin panel
*** 
[![admin.png](https://i.postimg.cc/c1nwWJym/admin.png)](https://postimg.cc/CdwzHSKB)
#### **Administrator permissions**
**Perform basic operations with cars:**
* Add new car
* Edit car
* Delete car
    
> The administrator receives information about orders by email.