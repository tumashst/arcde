USE [CarShop]
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'Admin')
GO
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'User')
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([Id], [Email], [RoleId], [ConfirmationToken], [IsConfirmed]) VALUES (1, N'admin@admin', 1, N'', 1)
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
INSERT [dbo].[PasswordHashes] ([UserId], [HashValue], [Salt]) VALUES (1, 0xC0C2C841EB265439927863EC17EBDFF2CB2D5E9A, 0x610DAFF5C09D20EF6EC36C1F02862232CA7F836CD5D834)
GO