﻿namespace Model.Office;

public class CarProduct : Product<Car>
{
    public CarProduct(Car car, int price, int count, string description, byte[] image) : base(car)
    {
        Price = price;
        Count = count;
        Description = description;
        Image = image;
    }

    public override string Name => $"{Subject.Mark} {Subject.Model}";

    public string Description { get; set; }

}