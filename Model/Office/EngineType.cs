﻿namespace Model.Office;

public class EngineType : IEquatable<EngineType>
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool Equals(EngineType other) => (other.Id, other.Name) == (Id, Name);
    public override string? ToString()
    {
        return Name;
    }
}