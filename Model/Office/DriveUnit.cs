﻿namespace Model.Office;

public class DriveUnit : IEquatable<DriveUnit> //привод
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool Equals(DriveUnit other) => (other.Id, other.Name) == (Id, Name);
    public override string? ToString()
    {
        return Name;
    }
}