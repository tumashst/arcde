﻿namespace Model.Office;

public class Color : IEquatable<Color>
{
    public int Id { get; set; }
    public int Red { get; set; }
    public int Green { get; set; }
    public int Blue { get; set; }

    public bool Equals(Color color) => (color.Red, color.Green, color.Blue) == (Red, Green, Blue);
}