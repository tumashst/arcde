﻿namespace Model.Office;

public class Transmission : IEquatable<Transmission>
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool Equals(Transmission other) => (other.Id, other.Name) == (Id, Name);
    public override string? ToString()
    {
        return Name;
    }
}