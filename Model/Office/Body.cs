﻿namespace Model.Office;

public class Body : IEquatable<Body>
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool Equals(Body other) => (other.Id, other.Name) == (Id, Name);
    public override string? ToString()
    {
        return Name;
    }
}