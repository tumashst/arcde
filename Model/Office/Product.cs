﻿using System.ComponentModel.DataAnnotations;

namespace Model.Office;

public abstract class Product<T>
{
    public int Id { get; set; }
    public Product(T subject) => Subject = subject;

    public byte[] Image { get; set; }
    [DisplayFormat(DataFormatString = "${0:n0}")]
    public int Price { get; set; }
    public int Count { get; set; }
    public abstract string Name { get; }
    public T Subject { get; set; }

    public override string ToString() => $"Name: {Name}, Count: {Count}, Price: {Price}";
}
