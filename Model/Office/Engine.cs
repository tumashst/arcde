﻿namespace Model.Office;

public class Engine : IEquatable<Engine>
{
    public int Id { get; set; }
    public virtual EngineType Type { get; set; }
    public float Capacity { get;set; }
    public bool Equals(Engine other) => (other.Type.Name, other.Capacity) == (Type.Name, Capacity);
    public override string? ToString()
    {
        return Type.ToString();
    }
}