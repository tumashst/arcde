﻿namespace Model.Office;

public class Car
{
    public Car(string mark, string model, int year, Body body, Transmission transmission, Engine engine, DriveUnit driveUnit,
        Color color)
    {
        Mark = mark;
        Model = model;
        Body = body;
        Transmission = transmission;
        Engine = engine;
        DriveUnit = driveUnit;
        Color = color;
        Year = year;
    }
    public int Id { get; set; }
    public int Year { get; set; }
    public string Mark { get; set; }
    public string Model { get; set; }
    public Body Body { get; set; }  //кузов
    public Transmission Transmission { get; set; } //коробка передач
    public Engine Engine { get; set; }
    public DriveUnit DriveUnit { get; set; }
    public Color Color { get; set; }


}