﻿namespace Model.Application;

public enum RoleName { Admin, User }