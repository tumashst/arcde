﻿namespace Model.Application;

public class PurchaseHistoryItem
{
    public int Id { get; set; }
    public DateTime PurchaseDate { get; set; }
    public string Mark { get; set; }
    public string Model { get; set; }
    public int TotalPrice { get; set; }
    public int Count { get; set; }
}