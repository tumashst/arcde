﻿namespace Model.Application;

public class Role
{
    public int Id { get; set; }
    public string Name { get; set; }

    public static Role Admin() => new() { Name = nameof(RoleName.Admin) };

    public static Role User() => new() { Name = nameof(RoleName.User) };

}