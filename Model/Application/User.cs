﻿namespace Model.Application;

public class User
{
    public int Id { get; set; }
    public string Email { get; set; }
    public bool IsConfirmed { get; set; }
    public string ConfirmationToken { get; set; }
    public PasswordHash PasswordHash { get; set; }
    public virtual Role Role { get; set; }
    public virtual List<PurchaseHistoryItem> PurchaseHistory { get; set; }
}