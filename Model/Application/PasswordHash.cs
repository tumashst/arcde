﻿namespace Model.Application;

public class PasswordHash
{
    public byte[] HashValue { get; set; }

    public byte[] Salt { get; set; }
}