﻿namespace Data.DataStorage;

public class CarProductDataStorage : IEntity
{
    public int Id { get; set; }

    public byte[] Image { get; set; }
    public int Price { get; set; }
    public int Count { get; set; }

    public virtual CarDataStorage Subject { get; set; }

    public string Description { get; set; }

}