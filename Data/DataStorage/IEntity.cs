﻿namespace Data.DataStorage;

public interface IEntity
{
    int Id { get; set; }
}