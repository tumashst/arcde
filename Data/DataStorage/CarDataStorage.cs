﻿using Model.Office;

namespace Data.DataStorage;

public class CarDataStorage : IEntity
{
    public int Id { get; set; }
    public int Year { get; set; }
    public string Model { get; set; }
    public virtual Body Body { get; set; }  //кузов
    public virtual Transmission Transmission { get; set; } //коробка передач
    public virtual Engine Engine { get; set; }
    public virtual DriveUnit DriveUnit { get; set; }
    public virtual Color Color { get; set; }

}