﻿using Microsoft.EntityFrameworkCore;
using Model.Application;

namespace Data.Sql
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<PasswordHash> PasswordHashes { get; set; }
        public DbSet<PurchaseHistoryItem> PurchaseHistoryItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder
                .UseSqlServer(
                    "Server=localhost;Initial Catalog=CarShop;Integrated Security=true;")
                .UseLazyLoadingProxies();
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
               .OwnsOne(r => r.PasswordHash);
        }

    }
}
