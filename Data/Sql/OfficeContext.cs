﻿using Data.DataStorage;
using Microsoft.EntityFrameworkCore;
using Model.Office;

namespace Data.Sql
{
    public class OfficeContext : DbContext
    {
        private readonly string _baseName;

        private readonly bool _lazy;

        public OfficeContext(string baseName)
        {
            _baseName = baseName.ToLower();
            Database.EnsureCreated();
        }
        public OfficeContext(string baseName, bool lazy) : this(baseName)
        {
            _lazy = lazy;
        }
        public DbSet<Body> Bodies { get; set; }
        public DbSet<CarDataStorage> Cars { get; set; }
        public DbSet<CarProductDataStorage> Products { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<DriveUnit> DriveUnits { get; set; }
        public DbSet<Engine> Engines { get; set; }
        public DbSet<EngineType> EngineTypes { get; set; }
        public DbSet<Transmission> Transmissions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Body>()
                .Property(p => p.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<Color>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<DriveUnit>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<EngineType>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<Engine>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<Transmission>()
                .Property(p => p.Id)
                .ValueGeneratedNever();


            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder
                .UseSqlServer(
                    $@"Server=localhost;Initial Catalog={_baseName};Integrated Security=true;")
                .UseLazyLoadingProxies(_lazy);
    }
}
