﻿using Auto.Interfaces;

namespace Data.Repository;

public class OutRepositoryProxy<T> : RepositoryProxy<T>
{
    private readonly Func<T, T> _proxy;

    public OutRepositoryProxy(IRepository<T> repository, Func<T, T> proxy) : base(repository)
    {
        _proxy = proxy;
    }

    protected override T In(T value) => value;

    protected override T Out(T value) => _proxy(value);

}