using Auto.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repository;

public class EFRepository<T> : IRepository<T> where T : class
{
    protected DbSet<T> Entities => DbContext.Set<T>();
    protected DbContext DbContext { get; }

    public EFRepository(DbContext dbContext) => DbContext = dbContext;

    public T Get(object id)
    {
        var entity = Entities.Find(id);
        DbContext.Entry(entity).State = EntityState.Detached;
        return entity;
    }

    public virtual T Add(T value)
    {
        Entities.Add(value);
        DbContext.SaveChanges();
        return value;
    }

    public virtual IEnumerable<T> All() => Entities.IncludeAll(DbContext).AsNoTracking();


    public virtual T Update(T value)
    {
        //TODO: ������� ���������
        try
        {
            DbContext.Attach(value);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        Entities.Update(value);
        DbContext.SaveChanges();
        return value;
    }

    public void Remove(T value)
    {
        DbContext.Attach(value);
        Entities.Remove(value);
        DbContext.SaveChanges();
    }

}
