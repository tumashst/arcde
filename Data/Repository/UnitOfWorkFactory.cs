using Auto.Interfaces;
using Shared;

namespace Data.Repository;

public class UnitOfWorkFactory : IFactory<string, IApplicationUnitOfWork>
{
    private readonly StorageConfiguration[] _configurations;

    public UnitOfWorkFactory(IEnumerable<StorageConfiguration> configurations) => _configurations = configurations.ToArray();

    private static IApplicationUnitOfWork CreateSql(string officeName) => new SqlUnitOfWork(officeName);


    public IApplicationUnitOfWork? CreateInstance(string userRequest)
    {
        var config = _configurations.First(configuration => configuration.Office == userRequest);
        return config.Type switch
        {
            StorageType.Json => throw new InvalidOperationException(),
            StorageType.Xml => throw new InvalidOperationException(),
            StorageType.Sql => CreateSql(userRequest),
            _ => throw new ArgumentOutOfRangeException()
        };

    }
}