﻿using Data.Sql;
using Model.Application;

namespace Data.Repository;

public class UserRepository : EFLazyRepository<User>
{
    public UserRepository(ApplicationContext dbContext) : base(dbContext)
    {
    }
}