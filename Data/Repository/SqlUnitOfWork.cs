using Auto.Interfaces;
using AutoMapper;
using Data.DataStorage;
using Data.Repository.Builder;
using Data.Sql;
using Model.Office;

namespace Data.Repository;

public class SqlUnitOfWork : IApplicationUnitOfWork
{

    private static readonly MapperConfiguration _config = new(cfg =>
    {
        cfg.CreateMap<CarDataStorage, Car>().ConstructUsing(carDataStorage => new Car(null, carDataStorage.Model, carDataStorage.Year, carDataStorage.Body, carDataStorage.Transmission, carDataStorage.Engine, carDataStorage.DriveUnit, carDataStorage.Color));
        cfg.CreateMap<Car, CarDataStorage>();
        cfg.CreateMap<CarProductDataStorage, CarProduct>().ConstructUsing((carProductDataStorage, res) => new CarProduct(res.Mapper.Map<Car>(carProductDataStorage.Subject), carProductDataStorage.Price, carProductDataStorage.Count, carProductDataStorage.Description, carProductDataStorage.Image));
        cfg.CreateMap<CarProduct, CarProductDataStorage>();
    });

    private static readonly IMapper _mapper = _config.CreateMapper();

    public SqlUnitOfWork(string tableName)
    {
        Products = new RepositoryBuilder<CarProductDataStorage>()
            .From(new CarProductEfRepository(new OfficeContext(tableName)))
            .WithMapper<CarProductDataStorage, CarProduct>(_mapper)
            .WithProxy(repository => new SetMarkProxy(repository, tableName))
            .Build();

        Transmissions = new RepositoryBuilder<Transmission>()
            .Sql()
            .WithName(tableName)
            .Lazy()
            .Build();

        EngineTypes = new RepositoryBuilder<EngineType>()
            .Sql()
            .WithName(tableName)
            .Lazy()
            .Build();

        DriveUnits = new RepositoryBuilder<DriveUnit>()
            .Sql()
            .WithName(tableName)
            .Build();

        Bodies = new RepositoryBuilder<Body>()
            .Sql()
            .WithName(tableName)
            .Build();
    }

    public IRepository<CarProduct> Products { get; }
    public IRepository<Transmission> Transmissions { get; }
    public IRepository<EngineType> EngineTypes { get; }
    public IRepository<DriveUnit> DriveUnits { get; }
    public IRepository<Body> Bodies { get; }
}