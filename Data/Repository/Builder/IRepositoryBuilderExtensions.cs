﻿using Auto.Interfaces;
using AutoMapper;

namespace Data.Repository.Builder;

public static class RepositoryBuilderExtensions
{
    public static IRepositoryBuilder<TTo> WithMapper<TFrom, TTo>(this IRepositoryBuilder<TFrom> repositoryBuilder,
        IMapper mapper) => new MappedRepositoryBuilder<TFrom, TTo>(repositoryBuilder, mapper);

    public static IRepositoryBuilder<T> WithProxy<T>(this IRepositoryBuilder<T> repositoryBuilder,
        Func<IRepository<T>, RepositoryProxy<T>> proxyFactory) =>
        new ProxiedRepositoryBuilder<T>(repositoryBuilder, proxyFactory);

    public static IRepositoryBuilder<T> WithProxy<T>(this IRepositoryBuilder<T> repositoryBuilder,
        Func<T, T> inProxy, Func<T, T> outProxy) => repositoryBuilder.WithInProxy(inProxy).WithOutProxy(outProxy);

    public static IRepositoryBuilder<T> WithInProxy<T>(this IRepositoryBuilder<T> repositoryBuilder, Func<T, T> inProxy) =>
        new ProxiedRepositoryBuilder<T>(repositoryBuilder, repository => new InRepositoryProxy<T>(repository, inProxy));

    public static IRepositoryBuilder<T> WithOutProxy<T>(this IRepositoryBuilder<T> repositoryBuilder, Func<T, T> outProxy) =>
        new ProxiedRepositoryBuilder<T>(repositoryBuilder, repository => new OutRepositoryProxy<T>(repository, outProxy));
}
