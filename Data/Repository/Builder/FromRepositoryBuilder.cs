﻿using Auto.Interfaces;

namespace Data.Repository.Builder;

public class FromRepositoryBuilder<T> : IRepositoryBuilder<T> where T : class
{
    private readonly IRepository<T> _repository;

    public FromRepositoryBuilder(IRepository<T> repository)
    {
        _repository = repository;
    }

    public IRepository<T> Build() => _repository;
}