﻿using Auto.Interfaces;

namespace Data.Repository.Builder;

public class ProxiedRepositoryBuilder<T> : IRepositoryBuilder<T>
{
    private readonly IRepositoryBuilder<T> _decorated;
    private readonly Func<IRepository<T>, RepositoryProxy<T>> _proxyFactory;

    public ProxiedRepositoryBuilder(IRepositoryBuilder<T> decorated, Func<IRepository<T>, RepositoryProxy<T>> proxyFactory)
    {
        _decorated = decorated;
        _proxyFactory = proxyFactory;
    }

    public IRepository<T> Build()
    {
        return _proxyFactory.Invoke(_decorated.Build());
    }
}