﻿using Auto.Interfaces;
using Data.DataStorage;

namespace Data.Repository.Builder;

public class RepositoryBuilder<T> : IRepositoryBuilder<T> where T : class
{
    public FileBasedRepositoryBuilder<TEntity> FileBased<TEntity>() where TEntity : class, IEntity
    {
        return new FileBasedRepositoryBuilder<TEntity>();
    }

    public SqlRepositoryBuilder<T> Sql()
    {
        return new SqlRepositoryBuilder<T>();
    }

    public IRepositoryBuilder<T> From(IRepository<T> repository)
    {
        return new FromRepositoryBuilder<T>(repository);
    }

    public IRepository<T> Build()
    {
        throw new InvalidOperationException();
    }
}