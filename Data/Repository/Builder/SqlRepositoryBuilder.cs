﻿using Auto.Interfaces;
using Data.Sql;

namespace Data.Repository.Builder;

public class LazySqlRepositoryBuilder<T> : IRepositoryBuilder<T> where T : class
{
    private string? _baseName;
    public LazySqlRepositoryBuilder<T> WithName(string name)
    {
        _baseName = name;
        return this;
    }

    public IRepository<T> Build()
        => new EFRepository<T>(new OfficeContext(_baseName));
}
public class SqlRepositoryBuilder<T> : IRepositoryBuilder<T> where T : class
{
    private string? _baseName;

    private bool _lazy;
    public SqlRepositoryBuilder<T> WithName(string name)
    {
        _baseName = name;
        return this;
    }
    public SqlRepositoryBuilder<T> Lazy()
    {
        _lazy = true;
        return this;
    }

    public IRepository<T> Build()
    {
        var context = new OfficeContext(_baseName, _lazy);

        return _lazy ? new EFLazyRepository<T>(context) : new EFRepository<T>(context);

    }

}