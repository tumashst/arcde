﻿using Data.Sql;
using Model.Application;

namespace Data.Repository;

public class RolesRepository : EFLazyRepository<Role>
{
    public RolesRepository(ApplicationContext dbContext) : base(dbContext)
    {

    }
}