using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Data.Repository;

public static class DbExtensions
{
    public static IQueryable<T> Include<T>(this IQueryable<T> source, IEnumerable<string> navigationPropertyPaths)
        where T : class =>
        navigationPropertyPaths.Aggregate(source, (query, path) => query.Include(path));

    public static IQueryable<T> IncludeAll<T>(this DbSet<T> entities, DbContext context) where T : class =>
        entities.Include(context.GetIncludePaths<T>());

    public static IEnumerable<string> GetIncludePaths<T>(this DbContext context, int maxDepth = int.MaxValue) =>
        context.GetIncludePaths(typeof(T), maxDepth);

    public static IEnumerable<string> GetIncludePaths(this DbContext context, Type clrEntityType,
        int maxDepth = int.MaxValue)
    {
        if (maxDepth < 0) throw new ArgumentOutOfRangeException(nameof(maxDepth));
        var entityType = context.Model.FindEntityType(clrEntityType);
        var includedNavigations = new HashSet<INavigation>();
        var stack = new Stack<IEnumerator<INavigation>>();
        while (true)
        {
            var entityNavigations = new List<INavigation>();
            if (stack.Count <= maxDepth)
            {
                foreach (var navigation in entityType.GetNavigations())
                {
                    if (includedNavigations.Add(navigation))
                        entityNavigations.Add(navigation);
                }
            }

            if (entityNavigations.Count == 0)
            {
                if (stack.Count > 0)
                    yield return string.Join(".", stack.Reverse().Select(e => e.Current.Name));
            }
            else
            {
                foreach (var navigation in entityNavigations)
                {
                    var inverseNavigation = navigation.Inverse;
                    if (inverseNavigation is not null)
                        includedNavigations.Add(inverseNavigation);
                }

                stack.Push(entityNavigations.GetEnumerator());
            }

            while (stack.Count > 0 && !stack.Peek().MoveNext())
                stack.Pop();
            if (stack.Count == 0) break;
            entityType = stack.Peek().Current.TargetEntityType;
        }
    }
}