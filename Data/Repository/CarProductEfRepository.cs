using Auto.Interfaces;
using Data.DataStorage;
using Microsoft.EntityFrameworkCore;
using Model.Office;

namespace Data.Repository;

public class CarProductEfRepository : IRepository<CarProductDataStorage>
{
    protected DbSet<CarProductDataStorage> Entities => DbContext.Set<CarProductDataStorage>();
    protected DbContext DbContext { get; }

    public CarProductEfRepository(DbContext dbContext) => DbContext = dbContext;

    public CarProductDataStorage Get(object id)
    {
        var entity = Entities.Find(id);
        DbContext.Entry(entity).State = EntityState.Detached;
        return entity;
    }


    public CarProductDataStorage Add(CarProductDataStorage value)
    {
        value.Subject.Body = DbContext.Set<Body>().Find(value.Subject.Body.Id);

        value.Subject.Engine.Type = DbContext.Set<EngineType>().Find(value.Subject.Engine.Type.Id);

        value.Subject.DriveUnit = DbContext.Set<DriveUnit>().Find(value.Subject.DriveUnit.Id);

        value.Subject.Transmission = DbContext.Set<Transmission>().Find(value.Subject.Transmission.Id);

        if (DbContext.Set<Engine>().Include(e => e.Type).FirstOrDefault(e => e.Equals(value.Subject.Engine)) is { } engine)
        {
            value.Subject.Engine = engine;
        }

        if (DbContext.Set<Color>().FirstOrDefault(c => c.Equals(value.Subject.Color)) is { } color)
        {
            value.Subject.Color = color;
        }

        DbContext.Add(value);
        DbContext.SaveChanges();
        DbContext.Entry(value).State = EntityState.Detached;

        return value;
    }

    public IEnumerable<CarProductDataStorage> All()
    {
        return Entities.IncludeAll(DbContext).AsNoTracking();
    }

    public void Remove(CarProductDataStorage value)
    {
        DbContext.Attach(value);
        Entities.Remove(value);
        DbContext.SaveChanges();
    }

    public CarProductDataStorage Update(CarProductDataStorage value)
    {
        value.Subject.Body = DbContext.Set<Body>().Find(value.Subject.Body.Id);

        var engine = DbContext.Set<CarProductDataStorage>()
            .Include(s => s.Subject)
            .ThenInclude(s =>s.Engine).First(s => s.Id == value.Id).Subject.Engine;

        engine.Type = DbContext.Set<EngineType>().Find(value.Subject.Engine.Type.Id);
        engine.Capacity = value.Subject.Engine.Capacity;
        value.Subject.Engine = engine;

        value.Subject.DriveUnit = DbContext.Set<DriveUnit>().Find(value.Subject.DriveUnit.Id);

        value.Subject.Transmission = DbContext.Set<Transmission>().Find(value.Subject.Transmission.Id);

        if (DbContext.Set<Color>().FirstOrDefault(c => c.Equals(value.Subject.Color)) is { } color)
        {
            value.Subject.Color = color;
        }

        try
        {
            DbContext.Attach(value);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        } 


        Entities.Update(value);
        DbContext.SaveChanges();
        DbContext.Entry(value).State = EntityState.Detached;
        return value;
    }
}