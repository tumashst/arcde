﻿using Auto.Interfaces;
using Model.Office;

namespace Data.Repository;

public class SetMarkProxy : OutRepositoryProxy<CarProduct>
{
    public SetMarkProxy(IRepository<CarProduct> repository, string mark) : base(repository, product =>
    {
        product.Subject.Mark = mark;
        return product;
    })
    {
    }
}
public abstract class RepositoryProxy<T> : IRepository<T>
{
    private readonly IRepository<T> _repository;

    public RepositoryProxy(IRepository<T> repository)
    {
        _repository = repository;
    }

    protected abstract T In(T value);

    protected abstract T Out(T value);

    public T Add(T value) => _repository.Add(In(value));

    public T Get(object key) => Out(_repository.Get(key));

    public IEnumerable<T> All() => _repository.All().Select(Out);

    public void Remove(T value) => _repository.Remove(In(value));

    public T Update(T value) => Out(_repository.Update(In(value)));

}