﻿using Auto.Interfaces;

namespace Data.Repository;

public class InRepositoryProxy<T> : RepositoryProxy<T>
{
    private readonly Func<T, T> _proxy;

    public InRepositoryProxy(IRepository<T> repository, Func<T, T> proxy) : base(repository)
    {
        _proxy = proxy;
    }

    protected override T In(T value) => _proxy(value);

    protected override T Out(T value) => value;

}