using Auto.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repository;

public class EFLazyRepository<T> : IRepository<T> where T : class
{
    protected DbSet<T> Entities => DbContext.Set<T>();
    protected DbContext DbContext { get; }

    public EFLazyRepository(DbContext context) => DbContext = context;
    public T Get(object id)
    {
        return Entities.Find(id);
    }

    public T Add(T value)
    {
        Entities.Add(value);
        DbContext.SaveChanges();
        return value;
    }

    public IEnumerable<T> All() => Entities;

    public void Remove(T value)
    {
        Entities.Remove(value);
        DbContext.SaveChanges();
    }

    public T Update(T value)
    {
        Entities.Update(value);
        DbContext.SaveChanges();
        return value;
    }
}