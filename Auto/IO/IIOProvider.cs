﻿using Model.Office;

namespace Auto.IO;

public interface IIOProvider
{
    void Write(CarProduct[] cars);
    void Write(EngineType[] engineTypes);
    void Write(Transmission[] transmissions);
    void Write(Body[] bodies);
    void Write(DriveUnit[] driveUnits);
    string ReadString();
    void Write(string @string);
    void Write(string[] strings);
}
