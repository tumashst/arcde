namespace Auto.Request;

public enum GetType { Models, Transmissions, Bodies, Cars, EngineTypes, DriveUnits, Marks }