using Auto.IO;
using Model.Office;

namespace Auto.Request;

public class UserRequest
{
    public UserRequestType Type { get; set; }

    public GetType GetType { get; set; }

    public IIOProvider IOProvider { get; set; }

    public UserRequest(UserRequestType type) => Type = type;

    public int Id { get; set; }
    public int Year { get; set; }
    public string Mark { get; set; }
    public int Count { get; set; }
    public int Price { get; set; }
    public byte[] Image { get; set; }
    public string Model { get; set; }
    public string Description { get; set; }
    public Body Body { get; set; }
    public Transmission Transmission { get; set; }
    public Engine Engine { get; set; }
    public DriveUnit DriveUnit { get; set; }
    public Color Color { get; set; }


}
