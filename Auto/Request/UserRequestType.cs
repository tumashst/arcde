﻿namespace Auto.Request;

public enum UserRequestType { Buy, Sell, Get, Help, Add, Remove, Update }
