﻿namespace Auto.Interfaces;

public interface IRepository<T>
{
    T Get(object id);
    T Add(T value);
    IEnumerable<T> All();
    void Remove(T value);
    T Update(T value);
}
