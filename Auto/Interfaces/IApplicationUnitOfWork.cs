using Model.Office;

namespace Auto.Interfaces;

public interface IApplicationUnitOfWork
{
    IRepository<CarProduct> Products { get; }

    IRepository<Transmission> Transmissions { get; }

    IRepository<EngineType> EngineTypes { get; }

    IRepository<DriveUnit> DriveUnits { get; }

    IRepository<Body> Bodies { get; }

}