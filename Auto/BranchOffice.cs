﻿using Auto.Interfaces;
using Model.Office;

namespace Auto;



internal class BranchOffice
{
    public string Mark { get; set; }

    public List<CarProduct> Products { get; }


    private readonly IRepository<CarProduct> _repository;
    private readonly IApplicationUnitOfWork _unitOfWork;

    public IEnumerable<Transmission> Transmissions => _unitOfWork.Transmissions.All();

    public IEnumerable<EngineType> EngineTypes => _unitOfWork.EngineTypes.All();

    public IEnumerable<DriveUnit> DriveUnits => _unitOfWork.DriveUnits.All();

    public IEnumerable<Body> Bodies => _unitOfWork.Bodies.All();

    public BranchOffice(string mark, IApplicationUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
        _repository = unitOfWork.Products;
        Mark = mark;
        Products = new(_repository.All());

    }

    internal void Buy(string model, int count)
    {
        var product = Products.FirstOrDefault(product => product.Subject.Model == model) ?? throw new ArgumentException($"Unknown model: \"{model}\"");

        product.Count += count;

        _repository.Update(product);
    }

    internal void Update(CarProduct carProduct)
    {
        var toUpdate = Products.FirstOrDefault(product => product.Id == carProduct.Id) ?? throw new ArgumentException($"Invalid id: \"{carProduct.Id}\""); ;

        Products.Remove(toUpdate);
        Products.Add(carProduct);

        _repository.Update(carProduct);
    }

    internal void Add(CarProduct carProduct)
    {
        Products.Add(carProduct);

        _repository.Add(carProduct);
    }

    internal void Remove(string model)
    {
        var product = Products.FirstOrDefault(product => product.Subject.Model == model) ?? throw new ArgumentException($"Unknown model: \"{model}\"");

        Products.Remove(product);

        _repository.Remove(product);
    }

    internal void Sell(string model, int count)
    {
        var product = Products.FirstOrDefault(product => product.Subject.Model == model) ?? throw new ArgumentException(nameof(model));

        if (product.Count >= count)
        {
            product.Count -= count;
            _repository.Update(product);
            return;
        }
        throw new ArgumentOutOfRangeException($"not possible to sell {count} of {model}; only {product.Count} in stock");
    }
}
