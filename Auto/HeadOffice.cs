﻿using Auto.Interfaces;
using Auto.IO;
using Auto.Request;
using Microsoft.Extensions.Logging;
using Shared.Logging;

namespace Auto;

public partial class HeadOffice
{
    private readonly IReadOnlyCollection<BranchOffice> _branchOffices;
    private readonly IUserRequestProcessor _requestProcessor;

    private readonly ILogger _logger;
    private readonly IIOProvider _ioProvider;

    private readonly string[] _branchOfficeNames = { "audi", "bmw" };
    public HeadOffice(IFactory<string, IApplicationUnitOfWork> repositoryFactory, IIOProvider ioProvider, ILogger? logger = null)
    {
        var audiUnitOfWork = repositoryFactory.CreateInstance(_branchOfficeNames[0]);
        var bmwUnitOfWork = repositoryFactory.CreateInstance(_branchOfficeNames[1]);

        _branchOffices = new BranchOffice[]
        {
            new(_branchOfficeNames[0], audiUnitOfWork),
            new(_branchOfficeNames[1], bmwUnitOfWork)
        };

        _logger = logger ?? Logger.Instance;
        _ioProvider = ioProvider;

        _requestProcessor = new RequestProcessor(logger, ioProvider, _branchOffices);

    }



    public void ProcessRequest(UserRequest request)
    {
        var unprocessed = _requestProcessor.ProcessRequest(request);
        if (unprocessed is not null)
        {
            throw new ArgumentException("Unable to create command from provided arguments");
        }
    }

}
