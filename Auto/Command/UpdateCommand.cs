﻿using Auto.Command.Arguments;
using Auto.Interfaces;
using Model.Office;

namespace Auto.Command;

internal class UpdateCommand : ICommand
{
    private readonly UpdateCommandArguments _arguments;
    private readonly BranchOffice _branchOffice;

    public UpdateCommand(BranchOffice branchOffice, UpdateCommandArguments arguments)
    {
        _arguments = arguments;
        _branchOffice = branchOffice;
    }

    public void Execute()
    {
        _branchOffice.Update(new CarProduct(
            new Car(_arguments.Mark, _arguments.Model, _arguments.Year, _arguments.Body, _arguments.Transmission, _arguments.Engine, _arguments.Unit, _arguments.Color),
            _arguments.Price, _arguments.Count, _arguments.Description, _arguments.Image)
        { Id = _arguments.Id });
    }
}