﻿using Auto.Command.Arguments;
using Auto.Interfaces;


namespace Auto.Command;

internal class GetCommand : ICommand
{
    private readonly BranchOffice[] _branchOffices;
    private readonly GetCommandArguments _arguments;

    public GetCommand(BranchOffice[] branchOffices, GetCommandArguments arguments)
    {
        _branchOffices = branchOffices;
        _arguments = arguments;
    }


    public void Execute()
    {
        switch (_arguments.GetType)
        {
            case Request.GetType.Cars:
                _arguments.IOProvider.Write(_branchOffices.SelectMany(office => office.Products).ToArray());
                break;
            case Request.GetType.Transmissions:
                _arguments.IOProvider.Write(_branchOffices.SelectMany(office => office.Transmissions).ToArray());
                break;
            case Request.GetType.Models:
                _arguments.IOProvider.Write(_branchOffices.SelectMany(office => office.Products).Select(product => product.Subject.Model).ToArray());
                break;
            case Request.GetType.Bodies:
                _arguments.IOProvider.Write(_branchOffices.SelectMany(office => office.Bodies).ToArray());
                break;
            case Request.GetType.EngineTypes:
                _arguments.IOProvider.Write(_branchOffices.SelectMany(office => office.EngineTypes).ToArray());
                break;
            case Request.GetType.DriveUnits:
                _arguments.IOProvider.Write(_branchOffices.SelectMany(office => office.DriveUnits).ToArray());
                break;
            case Request.GetType.Marks:
                _arguments.IOProvider.Write(_branchOffices.Select(office => office.Mark).ToArray());
                break;


        }

    }


}
