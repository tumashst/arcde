﻿using Auto.Interfaces;
using Auto.Request;

namespace Auto.Command.Factory;

internal class GetCommandFactory : IFactory<UserRequest, ICommand>
{
    private readonly BranchOffice[] _branchOffices;

    public GetCommandFactory(BranchOffice branchOffice)
    {
        _branchOffices = new[] { branchOffice };
    }

    public GetCommandFactory(IReadOnlyCollection<BranchOffice> offices)
    {
        _branchOffices = offices.ToArray();
    }
    public ICommand? CreateInstance(UserRequest userRequest)
        => new GetCommand(_branchOffices, new Arguments.GetCommandArguments(userRequest.IOProvider, userRequest.GetType));
}
