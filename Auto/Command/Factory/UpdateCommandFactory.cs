﻿using Auto.Command.Arguments;
using Auto.Interfaces;
using Auto.Request;

namespace Auto.Command.Factory;

internal class UpdateCommandFactory : IFactory<UserRequest, ICommand>
{
    private readonly BranchOffice _branchOffice;

    public UpdateCommandFactory(BranchOffice branchOffice)
    {
        _branchOffice = branchOffice;
    }

    public ICommand? CreateInstance(UserRequest userRequest)
        => new UpdateCommand(_branchOffice,
            new UpdateCommandArguments(userRequest.Id, userRequest.Mark, userRequest.Model, userRequest.Count,
                userRequest.Price, userRequest.Year, userRequest.Description,
                userRequest.Body, userRequest.Engine, userRequest.DriveUnit,
                userRequest.Transmission, userRequest.Color, userRequest.Image));
}