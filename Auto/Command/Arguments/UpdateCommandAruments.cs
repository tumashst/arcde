﻿using Model.Office;

namespace Auto.Command.Arguments;

record UpdateCommandArguments(int Id, string Mark, string Model, int Count, int Price, int Year,
    string Description, Body Body, Engine Engine, DriveUnit Unit,
    Transmission Transmission, Color Color, byte[] Image);
