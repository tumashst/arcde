﻿using Auto.IO;
using Auto.Request;

namespace Auto.Command.Arguments;

internal class GetCommandArguments
{
    public GetType GetType { get; }
    public IIOProvider IOProvider { get; }

    public GetCommandArguments(IIOProvider IOProvider, GetType type)
    {
        GetType = type;
        this.IOProvider = IOProvider;
    }

}
