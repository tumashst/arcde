﻿using Auto.Command.Arguments;
using Auto.Interfaces;
using Model.Office;

namespace Auto.Command;

internal class AddCommand : ICommand
{
    private readonly AddCommandArguments _arguments;
    private readonly BranchOffice _branchOffice;

    public AddCommand(BranchOffice branchOffice, AddCommandArguments arguments)
    {
        _arguments = arguments;
        _branchOffice = branchOffice;
    }

    public void Execute()
    {
        _branchOffice.Add(
            new CarProduct(
                new Car(_branchOffice.Mark, _arguments.Model, _arguments.Year, _arguments.Body, _arguments.Transmission, _arguments.Engine, _arguments.Unit, _arguments.Color),
                _arguments.Price, _arguments.Count, _arguments.Description, _arguments.Image));
    }
}